import React from 'react'
import { Provider } from 'react-redux'
import store from './store'

import './App.css'
import 'toastr/build/toastr.css'
import Loader from './components/loader/loader'
import ReactGA from 'react-ga'
import {GoogleReCaptchaProvider} from "react-google-recaptcha-v3";
function App() {
  ReactGA.initialize('UA-62287527-1')
  ReactGA.pageview('/calculadora-de-calorias')
  return (
    <Provider store={store}>

      <Loader />
    </Provider>
  )
}

export default App
