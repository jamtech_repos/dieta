import React, { Component } from 'react'
import { connect } from 'react-redux'
import './arrow.css'
class Arrow extends Component {
  validateNext() {
    switch (this.props.page) {
      case 1: {
        return this.props.sexo !== ''
      }
      case 2: {
        return this.props.actividad_fisica !== ''
      }
      case 3: {
        return this.props.carnes.indexOf(true) >= 0
      }
      case 4: {
        return this.props.alergias.indexOf(true) >= 0
      }
      case 5: {
        return this.props.maloshabitos.indexOf(true) >= 0
      }
      case 6: {
        return this.props.objetivo > 0
      }
      default:
        break
    }
  }
  render() {
    return (
      <div className="arrows">
        <ul>
          {this.props.page !== 7 ? (
              <>
                <li
                  className="disable"
                  onClick={() => this.props.changePage(this.props.page - 1)}
                >
                  <i className="fas fa-angle-left"/> Anterior
                </li>

                <li
                  className={'next ' + (this.validateNext() ? 'enable' : 'disable')}
                  onClick={() => {
                    if (this.validateNext())
                      this.props.changePage(this.props.page + 1)
                  }}
                >
                  Siguiente <i className="fas fa-angle-right"/>
                </li>
              </>
          ) : null}
        </ul>
      </div>
    )
  }
}
const mapStateToProps = state => ({
  page: state.page,
  sexo: state.sexo,
  actividad_fisica: state.actividad_fisica,
  carnes: state.carnes,
  alergias: state.alergias,
  maloshabitos: state.maloshabitos,
  objetivo: state.objetivo,
})
const mapDispatchToProps = dispatch => ({
  changePage(page) {
    dispatch({
      type: 'CHANGE_PAGE',
      page: page,
    })
  },
})
export default connect(mapStateToProps, mapDispatchToProps)(Arrow)
