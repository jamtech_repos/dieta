import React, { Component } from 'react'
import news1 from '../../assets/img/news1.png'
import news2 from '../../assets/img/news2.png'
import news3 from '../../assets/img/news3.png'
import news4 from '../../assets/img/news4.png'
import transformacion1 from '../../assets/img/transformacion1.png'
import transformacion2 from '../../assets/img/transformacion2.png'
import transformacion3 from '../../assets/img/transformacion3.png'
import transformacion4 from '../../assets/img/transformacion4.png'
import transformacion5 from '../../assets/img/transformacion5.png'
import transformacion6 from '../../assets/img/transformacion6.png'
import transformacion7 from '../../assets/img/transformacion7.png'
import transformacion8 from '../../assets/img/transformacion8.png'
import transformacion9 from '../../assets/img/transformacion9.png'
import transformacion10 from '../../assets/img/transformacion10.png'
import transformacion11 from '../../assets/img/transformacion11.png'
import transformacion12 from '../../assets/img/transformacion12.png'
import transformacion13 from '../../assets/img/transformacion13.png'
import transformacion14 from '../../assets/img/transformacion14.png'
import transformacion15 from '../../assets/img/transformacion15.png'
import './footer.css'

class FooterInfo extends Component {
  render() {
    return (
      <section className="container">
        <div className="info-title">
          <h1 style={{ marginBottom: '0px' }}>¿Quieres aprender a entrenar</h1>
          <h1 style={{ marginTop: '0px' }}>de forma efectiva?</h1>
          <a target="_blank" href="https://fullmusculo.com/">
            si quiero
          </a>
        </div>
        <div className="info-images">
          <div className="colum-images">
            <img src={transformacion14} alt="foto" />
            <img src={transformacion7} alt="foto" />

            <img src={transformacion13} alt="foto" />
            <img src={transformacion8} alt="foto" />
            <img src={transformacion2} alt="foto" />
            <img src={transformacion12} alt="foto" />
            <img src={transformacion4} alt="foto" />
            <img src={transformacion11} alt="foto" />
            <img src={transformacion6} alt="foto" />
            <img src={transformacion1} alt="foto" />
            <img src={transformacion10} alt="foto" />
            <img src={transformacion5} alt="foto" />
            <img src={transformacion9} alt="foto" />
            <img src={transformacion15} alt="foto" />
            <img src={transformacion3} alt="foto" />
          </div>
        </div>
        <div className="info-description container">
          <h1>Apúntate a nuestro centro total de entrenamiento online</h1>
          <h5 className="info-description__subtitle">
            El CTE de FullMusculo es un espacio donde las personas normales
            consiguen resultados increíbles
          </h5>
          <h5>
            Tanto si estás entrenando en casa como si entrenas en el gym, en el
            CTE tendrás toda la informacion, las herramientas y el apoyo
            necesario para que puedas entrenar de forma efectiva y alcanzar tus
            objetivos de forma saludable y sotenibles en el tiempo.
          </h5>
        </div>
        <div className="info-news container">
          <h1>Hemos sido presentados por</h1>
          <div className="images-container">
            <img src={news1} alt="news" />
            <img src={news2} alt="news" style={{ width: '170px' }} />
            <img src={news3} alt="news" style={{ width: '290px' }} />
            <img src={news4} alt="news" />
          </div>
        </div>
      </section>
    )
  }
}

export default FooterInfo
