import React, {Component} from 'react'
import './footer.css'


class Footer extends Component {
    render() {
        return (
            <footer>
                <div className="menu-footer">
                    <span><a href="https://fullmusculo.com/login">© 2023 FullMusculo ™ · Fitness basado en ciencia</a></span>
                    <ul>
                        <li><a href="https://fullmusculo.com/politica-de-privacidad/">Política de privacidad</a></li>
                        <li><a href="https://fullmusculo.com/terminos-de-uso/">Términos de uso</a></li>
                        <li><a href="https://fullmusculo.com/about/">Quienes somos</a></li>
                        <li><a href="https://fullmusculo.com/colabora-con-fullmusculo/">Colaborariones</a></li>
                    </ul>
                </div>
            </footer>
        );
    }
}

export default Footer;