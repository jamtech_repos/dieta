import React, { Component } from 'react'
import { connect } from 'react-redux'
import dots from '../../assets/three-dots.svg'
import logo from '../../assets/img/logoHeader.svg'

import Header from '../header/header'
import Footer from '../footer/footer'
import Page from '../../pages/pages'
import './loader.css'
class Loader extends Component {
  constructor(props) {
    super(props)
    this.state = { textloader: '' }
    this.activeLoader = this.activeLoader.bind(this)
  }
  componentDidUpdate() {
    if (this.props.page === 12 && this.state.textloader === '') {
      localStorage.setItem('dataCalc',JSON.stringify(this.props.state))
      this.activeLoader()
    }
  }
  activeLoader() {
    let i = 1
    const interval = setInterval(() => {
      switch (i) {
        case 1:
          this.setState({ textloader: 'Revisando tus opciones' })
          break
        case 2:
          this.setState({ textloader: 'Calculando calorias' })
          break
        case 3:
          this.setState({ textloader: 'Generando Macronutrientes' })
          break
        case 4:
          this.setState({ textloader: 'Realizando recomendaciones' })
          break
        case 5:
          this.setState({ textloader: 'Buscando tu peso ideal' })
          break
        default:
          break
      }
      if (i >= 5) {
        clearInterval(interval)
        this.props.changePage()
      }
      i++
    }, 1000)
  }
  render() {
    return (
      <>
        {this.props.page === 12 ? (
          <div className="loader-content">
            <div className="backdrop"></div>
            <img className="loader2 logo" src={logo} alt="logo" />
            <img className="loader2" src={dots} alt="loader" />
            <div className="text">
              <p>{this.state.textloader}</p>
            </div>
          </div>
        ) : (
          <div>
            <Header />
            <div>
              <Page />
            </div>

            <Footer />
          </div>
        )}
      </>
    )
  }
}
const mapStateToProps = state => ({
  page: state.page,
  state: state
})
const mapDispatchToProps = dispatch => ({
  changePage() {
    dispatch({
      type: 'CHANGE_PAGE',
      page: 8,
    })
  },
})
export default connect(mapStateToProps, mapDispatchToProps)(Loader)
