import React, { Component } from 'react';
import logo from '../../assets/img/logoHeader.svg';
import './header.css';
import {Button, Container} from "react-bootstrap";

class Header extends Component {
  constructor() {
    super();
    this.state = {
      userRestricId: localStorage.getItem("userRestricId")
    }
  }
  render() {
    return (
      <header>
        <nav>
          <Container>
            <div className="nav row">
              <div className="logo">
                <a href="https://fullmusculo.com/">
                  <img width={689} height={175} href="https://fullmusculo.com/" src={logo} alt=""></img>
                </a>
              </div>
              <div className="main-menu">
                <ul>
                  <li>
                    <a href="https://fullmusculo.com/blog/">Blog</a>
                  </li>
                  <li>
                    <a href="https://fullmusculo.com/podcast-fitness/">Podcast</a>
                  </li>
                  <li>
                    <a href="https://fullmusculo.com/academy/">Academia</a>
                  </li>
                  {this.state.userRestricId ?<li>
                    <a href="https://fullmusculo.com/account/">Cuenta</a>
                  </li>:null}
                </ul>
              </div>
              {!this.state.userRestricId ? <div className="main-menu">
                <ul>
                  <li>
                    <a className={"btn-primary"} href="https://fullmusculo.com/account/">Subscribete</a>
                  </li>
                  <li>
                    <a href="https://fullmusculo.com/account/">Login</a>
                  </li>
                </ul>
              </div>:null}
            </div>
          </Container>
        </nav>
      </header>
    );
  }
}

export default Header;
