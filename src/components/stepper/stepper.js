import React, { Component } from 'react';
import './stepper.css';
import { connect } from 'react-redux';

class Stepper extends Component {
  setActive(i) {
    if (this.props.page === i) {
      return 'step-active';
    } else {
      if (i < this.props.page) {
        return 'step-done';
      } else {
        return null;
      }
    }
  }
  render() {
    return (
      <div className='stepperComponent'>
        <div className='stepper'>
          <div
            className={'step ' + this.setActive(1)}
            data-placement='right'
            data-toggle='popover'
            data-trigger='hover'
            title='Genero Sexual'>
            <div>
              <div className='circle'>1</div>
            </div>
          </div>
          <div
            className={'step ' + this.setActive(2)}
            data-placement='right'
            data-toggle='popover'
            data-trigger='hover'
            title='Actividad Física'>
            <div>
              <div className='circle'>2</div>
            </div>
          </div>
          <div
            className={'step ' + this.setActive(3)}
            data-placement='right'
            data-toggle='popover'
            data-trigger='hover'
            title='Carnes'>
            <div>
              <div className='circle'>3</div>
            </div>
          </div>
          <div
            className={'step ' + this.setActive(4)}
            data-placement='right'
            data-toggle='popover'
            data-trigger='hover'
            title='Alergias y/o intolerancias'>
            <div>
              <div className='circle'>4</div>
            </div>
          </div>
          <div
            className={'step ' + this.setActive(5)}
            data-placement='right'
            data-toggle='popover'
            data-trigger='hover'
            title='Malos Hábitos'>
            <div>
              <div className='circle'>5</div>
            </div>
          </div>
          <div
            className={'step ' + this.setActive(6)}
            data-placement='right'
            data-toggle='popover'
            data-trigger='hover'
            title='Objetivo'>
            <div>
              <div className='circle'>6</div>
            </div>
          </div>
          <div
            className={'step ' + this.setActive(7)}
            data-placement='right'
            data-toggle='popover'
            data-trigger='hover'
            title='Medidas'>
            <div>
              <div className='circle'>7</div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}
const mapStateToProps = state => ({
  page: state.page
});
const mapDispatchToProps = dispatch => ({
  changePage(page) {
    dispatch({
      type: 'CHANGE_PAGE',
      page: page
    });
  }
});
export default connect(mapStateToProps, mapDispatchToProps)(Stepper);
