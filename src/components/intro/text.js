import React from 'react';

const Text = () => {
  return (
    <div id="textsection" className='container' style={{ color: 'white', marginTop: '10%',paddingBottom:'400px' }}>
      <div className='col-sm-12'>
        <h1
          style={{
            textAlign: 'center',
            marginBottom: '10%',
            textTransform: 'uppercase'
          }}>
          Cómo funciona nuestra Calculadora de calorías?
        </h1>
        <p>
          Con nuestra calculadora de calorías y macronutrientes podrás analizar
          tu condición física actual en segundos de forma gratuita.
        </p>
        <p>
          Determinaremos cual es tu índice de masa corporal o IMC, tu peso ideal
          (ambos como referencia), cuántas calorías debes consumir según tu objetivo,
          la cantidad de proteínas, carbohidratos y grasas que debes incluir en
          tu alimentación diaria (en gramos) y tu máximo potencial muscular (solo en hombres).
        </p>
        {/* <!--  --> */}
        <h1>
          Nuestra herramienta para calcular el IMC y la calculadora de calorías
        </h1>
        <p>
          Ante todo es importante que sepas que nuestro objetivo con esta
          herramienta es que puedas calcular tu IMC (Índice de masa corporal)
          actual y el consumo calórico diario que permitirá lograr tu objetivo:
          Ganar masa muscular, mantenimiento o Definición muscular.
        </p>
        <p>
          Nuestra calculadora de calorías utiliza las fórmulas más precisas que
          existen hoy en día para poder darte resultados confiables.
        </p>
        <p>
          La ciencia hoy nos permite, a través de la calculadora de calorías y
          la calculadora de IMC, determinar el consumo calórico diario sin tener
          que llevar a cabo un examen clínico.
        </p>
        <p>
          Para calcular tu IMC y las calorías según tu objetivo vamos a
          necesitar datos como el género, edad, altura, peso y nivel de
          actividad física actual.
        </p>
        <p>
          Esta calculadora te permitirá conocer la clasificación
          según tu Índice de masa corporal, tu peso ideal, El máximo potencial
          muscular (En el caso de los hombres), entre otros.
        </p>
        <p>
          Pero es importante tener en cuenta que
          sólo es capaz de predecir un promedio de tu gasto calórico diario
          basándose en los factores antes mencionados (género, peso, altura,
          etc) y el nivel de actividad física diaria.
        </p>
        <p>
          Nuestra herramienta no toma en cuenta factores de igual importancia
          como el descanso, el estrés, la calidad de las calorías, el balance
          calórico, etc.
        </p>
        <p>
          Por lo tanto debes tomar en cuenta que una predicción basada solo en
          las calorías que gastas y consumes no es suficiente para determinar tu
          estado de salud actual.
        </p>
        <p>
          El tipo de alimentos y la diversidad de tu alimentación es lo que
          permitirá consumir todos los nutrientes que tu organismo necesita.
        </p>
        <p>
          Si deseas aprender a cómo llevar una vida saludable puedes ir a
          nuestro blog o buscar alguno de nuestros programas o asesorías que se
          adapte a tus necesidades.
        </p>
        <p>
          Ten en cuenta que de nada te servirá calcular el IMC actual, ni usar
          nuestra calculadora de calorías si no eres capaz de hacer un cambio de
          hábitos en tu estilo de vida.
        </p>
        {/* <!--  --> */}
        <h1>Las fórmulas de nuestra calculadora de calorías</h1>
        <p>Lo primero que calculamos es la tasa Metabólica Basal o BMR.</p>
        <p>
          Para ello en nuestra herramienta para calcular IMC y la calculadora de
          calorías hemos elegido la que al día de hoy es la fórmula más precisa.
        </p>
        <h2>La formula de Mifflin - St Jeor</h2>
        <p>
          <b>Para Hombres </b> BMR = 9.99 x Peso en kg + 6.25 x Altura en cm -
          4.92 x Edad + 5
        </p>
        <p>
          <b>Para Mujeres </b> BMR = 9.99 x Peso en kg + 6.25 x Altura en cm -
          4.92 x Edad - 161
        </p>
        <p>
          Para esta fórmula debemos usar el peso en kilogramos, la altura en
          centímetros y la edad en años.
        </p>
        <p>
          Para calcular las calorías según el nivel de actividad física se debe
          multiplicar el TMB o BMR por el factor que corresponda según lo
          seleccionado en la calculadora:
        </p>
        <div style={{ paddingLeft: '40px' }}>
          <h3>Sedentario </h3>
          <p>
            Se considera sedentario a una persona con profesión que requiera
            estar sentado todo el día con desplazamientos o caminatas cortas
            esporádicas. Con poco o nada de ejercicio.
          </p>
          <p>BMR x 1.2.</p>
          <h3>Ligeramente activo</h3>
          <p>
            Persona con una profesión que requiera estar sentado todo el día.
            Esta persona se moviliza a pie o en bicicleta y realiza actividad
            física moderada sin entrenamiento (Por ejemplo: Una hora de caminata
            diaria). O para una persona que entrene o haga deporte 1-3 
            días por semana.
          </p>
          <p>BMR x 1.37</p>
          <h3>Moderado/ Activo</h3>
          <p>
            Persona con un trabajo activo que también se moviliza a pie y
            realiza entre 90 y 120 minutos de actividad física sin
            entrenamiento. O para una persona que entrene o haga deporte 3-5 
            días por semana.
          </p>
          <p>BMR x 1.55</p>
          <h3>Muy Activo</h3>
          <p>
            Personas con profesiones muy activas (por ejemplo empleado de la
            construcción) con actividad física sin entrenamiento equivalente a
            correr 15-20 kilometros diarios. O para una persona que entrene 
            o haga deporte 6-7 días por semana.
          </p>
          <p>BMR x 1.73</p>
          <h3>Atleta</h3>
          <p>
            Profesiones extremadamente activas o atletas profesionales que
            requieren entrenamiento 2 veces al día, o el equivalente a
            correr 22-25 km diarios.
          </p>
          <p>BMR x 1.9</p>
        </div>
        <p>
          En nuestra herramienta para calcular el IMC y la calculadora de
          calorías tendrás presente las 5 opciones para que puedas elegir la que
          mejor se adapta a tu estilo de vida actual y nivel de actividad
          física.
        </p>

        {/* <!--  --> */}
        <h1>¿Cómo calcular el IMC?</h1>
        <p>
          Primero debes tener en cuenta que los nutricionistas calculan el IMC
          para estimar si su paciente sufre de sobrepeso, obesidad o bajo peso,
          teniendo en consideración la altura y su peso actual.
        </p>
        <p>
          La fórmula que hemos usado en nuestra calculadora de calorias para
          calcular el IMC es la siguiente:
        </p>
        <p style={{ paddingLeft: '40px' }}>
          <b>BMI</b> = (Peso en Kg / (Altura en metros x Altura en metros))
        </p>
        <p>
          Con el resultado anterior y usando la siguiente clasificación podemos
          obtener la categoría del BMI/IMC
        </p>
        <p style={{ paddingLeft: '40px' }}>
          <b>Bajo peso</b> = menos de 18.5
        </p>
        <p style={{ paddingLeft: '40px' }}>
          <b>Peso normal</b> = 18.5 – 24.9
        </p>
        <p style={{ paddingLeft: '40px' }}>
          <b>Sobrepeso</b> = 25 – 29.9
        </p>
        <p style={{ paddingLeft: '40px' }}>
          <b>Obesidad</b> = IMC de 30 ó mayor
        </p>
        <p>Luego Calculamos el peso ideal con la fórmula de J.D Robinson:</p>
        <p style={{ paddingLeft: '40px' }}>
          <b>En hombres:</b> Peso ideal en Kg = 52 kg + 1.9 Kg por cada pulgada
          por encima de los 5 pies.
        </p>
        <p style={{ paddingLeft: '40px' }}>
          <b>En mujeres:</b> Peso ideal en Kg = 49 kg + 1.7 Kg por cada pulgada
          por encima de los 5 pies.
        </p>
        <p>
          Luego en el caso de los hombres procedemos a calcular el máximo
          potencial muscular usando la fórmula de Martin Berkhan:
        </p>
        <p style={{ paddingLeft: '40px' }}>
          <b>MPM Rango superior:</b> Altura en centímetros - 98
        </p>
        <p style={{ paddingLeft: '40px' }}>
          <b>MPM Rango inferior:</b> Altura en centímetros - 102
        </p>
        <p>Y el resultado se muestra de la siguiente forma:</p>
        <p style={{ paddingLeft: '40px' }}>
          El máximo potencial muscular es <b>MPM inferior</b> a{' '}
          <b>MPM superior</b> con un 5-6% de grasa corporal.
        </p>
        <p>Por último calculamos los macronutrientes:</p>
        <p>
          Te mostramos tus requerimientos proteicos en gramos por kilo de peso
          corporal al día. Entre 1 y 3 gr/kg/día que puedes modificar desde 
          la página de los resultados.
          Tus grasas irán desde un 15 a un 80% de tu total de calorías y también
          puedes modificarlas.
          Y por último los carbohidratos que se ajustan automáticamente según
          como modifiques los macronutrientes anteriores.
        </p>
        <p>
          Debes tener en cuenta que hay 4 calorías por cada gramo de proteína 
          y de carbohidratos. Y 9 calorías por cada gramo de grasa.
        </p>
        {/* <!--  --> */}
        <h1>¿Cómo calcular las calorías según tu objetivo?</h1>
        <div style={{ paddingLeft: '40px' }}>
          <h3>Para definición muscular</h3>
          <p>
            Cuando tu objetivo sea el de perder peso entonces será necesario
            definir un déficit calórico.
          </p>
          <p>
            Pero cuando calculamos tu déficit lo hacemos tomando en cuenta tu
            nivel de actividad física actual.
          </p>
          <p>
            Sin crear restricciones que además de no ser saludables, evitan la
            adherencia a un plan alimenticio que permita reducir los depósitos
            grasos del cuerpo.
          </p>
          <p>
            Por esa razón no debemos reducir nuestra ingesta de calorías a menos
            del 80% del TDEE (según sea el caso).
          </p>
          <p>
            Un déficit mayor al recomendado puede ocasionar una perdida de peso
            en masa muscular no deseada.
          </p>
          <h3>Para mantenimiento</h3>
          <p>
            Si tu elección en nuestra calculadora de calorías ha sido la de
            mantenimiento es porque consideras que tu estado físico actual es
            saludable.
          </p>
          <p>
            Para ello te recomendamos una ingesta de calorías sin déficit ni
            superávit calórico pero siempre tomando en cuenta el nivel de
            actividad física diario para ajustar tu requerimiento proteico.
          </p>
          <h3>Para Aumentar masa muscular</h3>
          <p>
            Si el objetivo que has elegido en nuestra calculadora de calorías es
            el de aumentar masa muscular entonces te recomendaremos un consumo
            mayor al de tu TDEE.
          </p>
          <p>
            Pero ten en cuenta que para lograr un aumento de masa muscular
            limpio este exceso de calorías debe provenir de alimentos de
            calidad, que provean de nutrientes y no solo de calorías vacías.
          </p>
          <p>
            Y que debes partir de un porcentaje de grasa óptimo, para iniciar
            una etapa de volumen, de entre un 8 y un 15% en hombres y entre
            un 16 y un 23% en mujeres.
          </p>
          <p>
            Un incremento del 20% de tu TDEE será suficiente para garantizar un
            aumento de masa muscular sin incrementar el porcentaje de grasa
            corporal.
          </p>
          <p>
            Siempre y cuando este vaya acompañado de un entrenamiento de fuerza
            adaptado a tus necesidades y objetivos, es decir, individualizado.
          </p>
        </div>
        <h1>
          Uso de nuestra herramienta para calcular IMC y calculadora de calorías
        </h1>
        <p>
          El uso de nuestra herramienta para calcular el IMC y calculadora de
          calorías es totalmente gratuito.
        </p>
        <p>
          Y para garantizar unos resultados precisos hemos seleccionado las
          mejores fórmulas que la ciencia al día de hoy puede ofrecer.
        </p>
        <p>
          A través de las fórmulas seleccionadas te podemos ofrecer un estimado
          bastante preciso de tu peso ideal (Que solo debes tomar como
          referencia), de tu IMC y del consumo de calorías orientados a tu
          objetivo.
        </p>
        <p>
          Espero que hagas uso responsable de nuestra herramienta y si te gusta
          puedes compartirla en tu blog y/o redes sociales siempre y cuando
          hagas nos des el crédito correspondiente mencionando nuestra página web.
        </p>
      </div>
    </div>
  );
};

export default Text;
