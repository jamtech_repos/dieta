import React, { Component } from 'react'
import Text from './text'
import { Button } from 'react-bootstrap'
import logo from '../../assets/img/logoHeader.svg'
import './intro.css'
import { connect } from 'react-redux'
import { createRef } from 'react'

class Intro extends Component {
  constructor(props) {
    super(props)
    this.state = { firstPageRef: createRef() }
    this.handleClick = this.handleClick.bind(this)
  }
  handleClick() {
    this.state.firstPageRef.current.scrollIntoView({
      behavior: 'smooth',
      block: 'start',
    })
  }
  render() {
    return (
      <div className="intro">
        <div className="intro__content">
          <img src={logo} alt="logo fullmusculo"></img>
          <h1>Calculadora de calorías y Macronutrientes</h1>
          <p style={{ color: 'white' }}>
            Te decimos totalmente GRATIS cuantas calorías, proteínas, grasas y
            carbohidratos necesitas según tu objetivo y te diseñamos una
            dieta personalizada.
          </p>
          <div
            id="buttons"
            className="row"
            style={{
              display: 'flex',
              justifyContent: 'center',
              margin: '50px',
            }}
          >
            <Button
              variant="outline-light"
              style={{ margin: '0px 10px' }}
              onClick={this.handleClick}
            >
              ¿Cómo funciona?
            </Button>
            <Button
              onClick={() => this.props.changePage(1)}
              style={{ margin: '0px 10px' }}
            >
              ¡Vamos allá!
            </Button>
          </div>
        </div>

        <div ref={this.state.firstPageRef}>
          <Text />
        </div>
      </div>
    )
  }
}
const mapStateToProps = state => ({
  page: state.page,
})
const mapDispatchToProps = dispatch => ({
  changePage(page) {
    dispatch({
      type: 'CHANGE_PAGE',
      page: page,
    })
  },
})
export default connect(mapStateToProps, mapDispatchToProps)(Intro)
