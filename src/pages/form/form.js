import React, { Component } from 'react'
import * as axios from 'axios'
import { connect } from 'react-redux'
import { Form, Button } from 'react-bootstrap'
import toastr from 'toastr'
import moment from 'moment'
import dots from '../../assets/three-dots.svg'
class FormPage extends Component {
  constructor(props) {
    super(props)
    this.state = {
      isclick: false,
      primer_nombre: '',
      apellido: '',
      email: '',
      como_te_enteraste: '',
      paypal_otro: '',
      sexo: '',
      edad: '',
      pais: '',
      altura: '',
      peso: '',
      peso_min_max: '',
      cintura_cadera: '',
      grasa_corporal: '',
      foto: null,
      cantidad_agua: '',
      tabaco: '',
      cafe_te: [false, false, false],
      evacuaciones: '',
      ansiedad: '',
      dia_tipico: '',
      entrenamiento_anteriormente: '',
      entrenamiento_desde_cuando: '',
      tipo_entrenamiento: '',
      experiencia_pesas: '',
      hora_entrenamiento: '',
      dias_semana_entrena: '1 día',
      tiempo_para_entrenar: '15 minutos',
      objetivos_espectativas: '',
      objetivo_detallado: '',
      comidas_dia: '',
      frutas_vegetales_consume: '',
      alimento_no_consume: '',
      desayuno: '',
      merienda_media_mañana: '',
      almuerzo: '',
      merienda_media_tarde: '',
      cena: '',
      merienda_antes_dormir: '',
      tomas_suplementos: '',
      suplementos_que_toma: '',
      alergias: '',
      enfermedad_cardiaca: '',
      dolor_de_pecho: '',
      dolor_de_pecho_sin_ejercicio: '',
      pierde_equilibrio: '',
      problemas_huesos: '',
      medicamento_tension: '',
      razon_no_ejercicio: '',
      impedimento: '',
      comentarios: '',
      whatsapp: '',
      onLoad: false,
    }
    this.handleUserInput = this.handleUserInput.bind(this)
    this.handleCafeTeChange = this.handleCafeTeChange.bind(this)
    this.sendDataToEmail = this.sendDataToEmail.bind(this)
    this.validateData = this.validateData.bind(this)
  }
  componentDidMount() {
    localStorage.setItem('page', 10)
    localStorage.setItem(
      'venc',
      moment()
        .add(1, 'days')
        .format()
    )
  }
  handleUserInput(e) {
    var name = e.target.name
    var value = e.target.value
    if (e.target.files) {
      this.setState({ [name]: e.target.files[0] })
    } else this.setState({ [name]: value })
  }
  handleCafeTeChange(i) {
    var cafe = this.state.cafe_te
    cafe[i] = !cafe[i]
    this.setState({ cafe_te: cafe })
  }
  createFormData() {
    var data = new FormData()
    data.append('primer_nombre', this.state.primer_nombre)
    data.append('apellido', this.state.apellido)
    data.append('email', this.state.email)
    data.append('como_te_enteraste', this.state.como_te_enteraste)
    data.append('paypal_otro', this.state.paypal_otro)
    data.append('sexo', this.state.sexo)
    data.append('edad', this.state.edad)
    data.append('pais', this.state.pais)
    data.append('altura', this.state.altura)
    data.append('peso', this.state.peso)
    data.append('peso_min_max', this.state.peso_min_max)
    data.append('cintura_cadera', this.state.cintura_cadera)
    data.append('grasa_corporal', this.state.grasa_corporal)
    data.append('foto', this.state.foto)
    data.append('cantidad_agua', this.state.cantidad_agua)
    data.append('tabaco', this.state.tabaco)
    data.append('cafe_te', this.state.cafe_te)
    data.append('evacuaciones', this.state.evacuaciones)
    data.append('ansiedad', this.state.ansiedad)
    data.append('dia_tipico', this.state.dia_tipico)
    data.append(
      'entrenamiento_anteriormente',
      this.state.entrenamiento_anteriormente
    )
    data.append(
      'entrenamiento_desde_cuando',
      this.state.entrenamiento_desde_cuando
    )
    data.append('tipo_entrenamiento', this.state.tipo_entrenamiento)
    data.append('experiencia_pesas', this.state.experiencia_pesas)
    data.append('hora_entrenamiento', this.state.hora_entrenamiento)
    data.append('dias_semana_entrena', this.state.dias_semana_entrena)
    data.append('tiempo_para_entrenar', this.state.tiempo_para_entrenar)
    data.append('objetivos_espectativas', this.state.objetivos_espectativas)
    data.append('objetivo_detallado', this.state.objetivo_detallado)
    data.append('comidas_dia', this.state.comidas_dia)
    data.append('frutas_vegetales_consume', this.state.frutas_vegetales_consume)
    data.append('alimento_no_consume', this.state.alimento_no_consume)
    data.append('desayuno', this.state.desayuno)
    data.append('merienda_media_mañana', this.state.merienda_media_mañana)
    data.append('almuerzo', this.state.almuerzo)
    data.append('merienda_media_tarde', this.state.merienda_media_tarde)
    data.append('cena', this.state.cena)
    data.append('merienda_antes_dormir', this.state.merienda_antes_dormir)
    data.append('tomas_suplementos', this.state.tomas_suplementos)
    data.append('suplementos_que_toma', this.state.suplementos_que_toma)
    data.append('alergias', this.state.alergias)
    data.append('enfermedad_cardiaca', this.state.enfermedad_cardiaca)
    data.append('dolor_de_pecho', this.state.dolor_de_pecho)
    data.append(
      'dolor_de_pecho_sin_ejercicio',
      this.state.dolor_de_pecho_sin_ejercicio
    )
    data.append('pierde_equilibrio', this.state.pierde_equilibrio)
    data.append('problemas_huesos', this.state.problemas_huesos)
    data.append('medicamento_tension', this.state.medicamento_tension)
    data.append('razon_no_ejercicio', this.state.razon_no_ejercicio)
    data.append('impedimento', this.state.impedimento)
    data.append('comentarios', this.state.comentarios)
    data.append('whatsapp', this.state.whatsapp)
    return data
  }
  sendDataToEmail() {
    this.setState({ isclick: true,onLoad:true })
    if (this.validateData()) {
      var bodyFormData = this.createFormData()
      var dieta =
        this.props.totalValue - this.props.planValue - this.props.dietaValue >=
        0
          ? true
          : false
      axios({
        method: 'post',
        url: 'https://fullmusculo.com/api/solicitudsenddieta?dieta=' + dieta,
        data: bodyFormData,
        headers: { 'Content-Type': 'multipart/form-data' },
      }).then(res => {
        const today =
          new Date().getDate() +
          '/' +
          (new Date().getMonth() + 1) +
          '/' +
          new Date().getFullYear()
        const ejercicio =
          this.props.totalValue -
            this.props.planValue -
            this.props.dietaValue >=
          0
        const data = {
          values: [
            [
              this.state.primer_nombre,
              this.state.apellido,
              this.state.sexo,
              this.state.email,
              this.state.pais,
              this.state.whatsapp,
              this.state.objetivos_espectativas,
              this.state.altura,
              this.state.peso,
              today.toString(),
              ejercicio ? 'Ejercicio agregado' : '',
            ],
          ],
        }
        const auth = {
          client_secret: 'U6ebXSM9HY6-pA2ZW9Q_veeH',
          grant_type: 'refresh_token',
          refresh_token:
            '1/G6o8z0CYBlWQ9m_8nZC1Rl97l377Z0Lwi_i-5tEOWw-OXxqOBtEtgeveqqRiiMyy',
          client_id:
            '157475155696-4td8juiqmplqv77j1kl154o0e42i4ann.apps.googleusercontent.com',
        }
        axios({
          method: 'post',
          url: 'https://www.googleapis.com/oauth2/v4/token',
          data: auth,
        }).then(res => {
          const token = res.data
          axios({
            method: 'post',
            url:
              'https://sheets.googleapis.com/v4/spreadsheets/1xzn1JPpXYAgXdzP_D6pvuOcM4peCv7GncvBGH9VTXIU/values/A1:append?includeValuesInResponse=false&insertDataOption=INSERT_ROWS&responseDateTimeRenderOption=SERIAL_NUMBER&responseValueRenderOption=FORMATTED_VALUE&valueInputOption=USER_ENTERED&key=AIzaSyCz9-fr9OgsercJM4hlDeLZKDksXXm8A04',
            data: data,
            headers: {
              Authorization: ' Bearer ' + token.access_token,
              'Content-Type': 'application/json',
              'cache-control': 'no-cache',
            },
          }).then(response => {
            window.location.replace('https://fullmusculo.com/calculadora-resultado')
          }).catch(
              err=> {
                this.setState({onLoad:false});
                toastr.error('Ha ocurrido un error de conexión con el servidor, puedes intentarlo mas tarde o contactarnos a través de info@fullmusculo.com')
              }
          )
        })
      }).catch(
          err=> {
            this.setState({onLoad:false});
            toastr.error('Ha ocurrido un error de conexión con el servidor, puedes intentarlo mas tarde o contactarnos a través de info@fullmusculo.com')
          }
      )
    } else {
      console.log('hi')
      toastr.clear()
      this.setState({onLoad:false})
      toastr.options = {
        showDuration: '3000',
        hideDuration: '1000',
        timeOut: '5000',
        extendedTimeOut: '1000',
        showEasing: 'swing',
        hideEasing: 'linear',
        showMethod: 'fadeIn',
        hideMethod: 'fadeOut',
      }
      toastr.error('Existen campos vacios o erroneos dentro del formulario')
    }
  }
  validateData() {
    if (
      this.state.primer_nombre !== '' &&
      this.state.apellido !== '' &&
      this.state.email !== '' &&
      this.state.email.match(/^([\w.%+-]+)@([\w-]+\.)+([\w]{2,})$/i) &&
      this.state.sexo !== '' &&
      this.state.edad !== '' &&
      this.state.pais !== '' &&
      this.state.altura !== '' &&
      this.state.peso !== '' &&
      this.state.grasa_corporal !== '' &&
      this.state.foto !== null &&
      this.state.cantidad_agua !== '' &&
      this.state.tabaco !== '' &&
      this.state.dia_tipico !== '' &&
      this.state.entrenamiento_anteriormente !== '' &&
      this.state.tipo_entrenamiento !== '' &&
      this.state.experiencia_pesas !== '' &&
      this.state.hora_entrenamiento !== '' &&
      this.state.dias_semana_entrena !== '' &&
      this.state.tiempo_para_entrenar !== '' &&
      this.state.objetivos_espectativas !== '' &&
      this.state.objetivo_detallado !== '' &&
      this.state.comidas_dia !== '' &&
      this.state.desayuno !== '' &&
      this.state.almuerzo !== '' &&
      this.state.cena !== '' &&
      this.state.tomas_suplementos !== '' &&
      this.state.alergias !== '' &&
      this.state.dolor_de_pecho !== '' &&
      this.state.dolor_de_pecho_sin_ejercicio !== '' &&
      this.state.pierde_equilibrio !== '' &&
      this.state.problemas_huesos !== '' &&
      this.state.medicamento_tension !== '' &&
      this.state.razon_no_ejercicio !== '' &&
      this.state.whatsapp !== ''
    )
      return true
    else return false
  }
  render() {
    return (
      <div
        className="container"
        style={{ padding: '0 10%', marginTop: '80px', marginBottom: '250px' }}
      >
        <h1>Tu dieta personalizada con FullMusculo</h1>
        <label>LEE y RELLENA ABSOLUTAMENTE TODO HASTA EL FINAL!</label>

        <Form>
          <h4>Formulario para tu plan de alimentación</h4>
          <div className="row">
            <div className="col-lg-8">
              <Form.Group>
                <Form.Label className="required">Primer nombre</Form.Label>
                <Form.Control
                  onChange={this.handleUserInput}
                  value={this.state.primer_nombre}
                  isInvalid={
                    this.state.isclick && this.state.primer_nombre === ''
                  }
                  name="primer_nombre"
                  type="text"
                  placeholder="Tu respuesta"
                ></Form.Control>
              </Form.Group>
              <Form.Group>
                <Form.Label className="required">Apellido</Form.Label>
                <Form.Control
                  onChange={this.handleUserInput}
                  value={this.state.apellido}
                  isInvalid={this.state.isclick && this.state.apellido === ''}
                  name="apellido"
                  type="text"
                  placeholder="Tu respuesta"
                ></Form.Control>
              </Form.Group>
              <Form.Group>
                <Form.Label className="required">
                  Email de contacto (Verificar que esté bien escrito)
                </Form.Label>
                <Form.Control
                  onChange={this.handleUserInput}
                  value={this.state.email}
                  isInvalid={
                    this.state.isclick &&
                    (this.state.email === '' ||
                      !this.state.email.match(
                        /^([\w.%+-]+)@([\w-]+\.)+([\w]{2,})$/i
                      ))
                  }
                  name="email"
                  type="text"
                  placeholder="Tu respuesta"
                ></Form.Control>
              </Form.Group>
              <Form.Group>
                <Form.Label>
                  Cómo te enteraste de nuestra promoción ?
                </Form.Label>
                <Form.Check
                  custom
                  checked={
                    this.state.como_te_enteraste ===
                    'Publicidad en el buscador de google'
                  }
                  id="como_te_enteraste1"
                  onChange={this.handleUserInput}
                  value="Publicidad en el buscador de google"
                  name="como_te_enteraste"
                  type="radio"
                  label={`Publicidad en el buscador de google `}
                />
                <Form.Check
                  custom
                  checked={this.state.como_te_enteraste === 'Instagram'}
                  id="como_te_enteraste2"
                  onChange={this.handleUserInput}
                  value="Instagram"
                  name="como_te_enteraste"
                  type="radio"
                  label={`Instagram `}
                />
                <Form.Check
                  custom
                  checked={this.state.como_te_enteraste === 'Vía Email'}
                  id="como_te_enteraste3"
                  onChange={this.handleUserInput}
                  value="Vía Email"
                  name="como_te_enteraste"
                  type="radio"
                  label={`Vía Email `}
                />
                <Form.Check
                  custom
                  checked={
                    this.state.como_te_enteraste ===
                    'Directamente en FullMusculo.com'
                  }
                  id="como_te_enteraste4"
                  onChange={this.handleUserInput}
                  value="Directamente en FullMusculo.com"
                  name="como_te_enteraste"
                  type="radio"
                  label={`Directamente en FullMusculo.com `}
                />
                <Form.Check
                  custom
                  id="como_te_enteraste5"
                  checked={
                    this.state.como_te_enteraste !==
                      'Publicidad en el buscador de google' &&
                    this.state.como_te_enteraste !== 'Instagram' &&
                    this.state.como_te_enteraste !== 'Vía Email' &&
                    this.state.como_te_enteraste !==
                      'Directamente en FullMusculo.com'
                  }
                  type="radio"
                  label={
                    <Form.Control
                      onChange={this.handleUserInput}
                      name="como_te_enteraste"
                      placeholder="Otro"
                      type="text"
                    ></Form.Control>
                  }
                />
              </Form.Group>
              <Form.Group>
                <Form.Label>
                  Si hiciste el pago desde la cuenta paypal de otra persona es
                  importante que nos indiques su nombre y correo electrónico:
                </Form.Label>
                <Form.Control
                  onChange={this.handleUserInput}
                  value={this.state.paypal_otro}
                  name="paypal_otro"
                  type="text"
                  placeholder="Tu respuesta"
                ></Form.Control>
              </Form.Group>
              <Form.Group>
                <Form.Label className="required">Sexo</Form.Label>
                <Form.Check
                  custom
                  checked={this.state.sexo === 'Mujer'}
                  isInvalid={this.state.isclick && this.state.sexo === ''}
                  id="sexo1"
                  onChange={this.handleUserInput}
                  value="Mujer"
                  name="sexo"
                  type="radio"
                  label={`Mujer `}
                />
                <Form.Check
                  custom
                  checked={this.state.sexo === 'Hombre'}
                  isInvalid={this.state.isclick && this.state.sexo === ''}
                  id="sexo2"
                  onChange={this.handleUserInput}
                  value="Hombre"
                  name="sexo"
                  type="radio"
                  label={`Hombre `}
                />
              </Form.Group>
              <Form.Group>
                <Form.Label className="required">Edad</Form.Label>
                <Form.Control
                  onChange={this.handleUserInput}
                  value={this.state.edad}
                  isInvalid={this.state.isclick && this.state.edad === ''}
                  name="edad"
                  type="text"
                  placeholder="Tu respuesta"
                ></Form.Control>
              </Form.Group>
              <Form.Group>
                <Form.Label className="required">
                  País y ciudad de donde nos escribes.
                </Form.Label>
                <Form.Control
                  onChange={this.handleUserInput}
                  value={this.state.pais}
                  isInvalid={this.state.isclick && this.state.pais === ''}
                  name="pais"
                  type="text"
                  placeholder="Tu respuesta"
                ></Form.Control>
              </Form.Group>
              <Form.Group>
                <Form.Label className="required">
                  Altura en centímetros (cm)
                </Form.Label>
                <Form.Control
                  onChange={this.handleUserInput}
                  value={this.state.altura}
                  isInvalid={this.state.isclick && this.state.altura === ''}
                  name="altura"
                  type="text"
                  placeholder="Tu respuesta"
                ></Form.Control>
              </Form.Group>
              <Form.Group>
                <Form.Label className="required">
                  Peso actual en Kilos (Kg)
                </Form.Label>
                <Form.Control
                  onChange={this.handleUserInput}
                  value={this.state.peso}
                  isInvalid={this.state.isclick && this.state.peso === ''}
                  name="peso"
                  type="text"
                  placeholder="Tu respuesta"
                ></Form.Control>
              </Form.Group>
              <Form.Group>
                <Form.Label>
                  Peso mínimo y máximo que has presentado en los últimos años
                </Form.Label>
                <Form.Control
                  onChange={this.handleUserInput}
                  value={this.state.peso_min_max}
                  name="peso_min_max"
                  type="text"
                  placeholder="Tu respuesta"
                ></Form.Control>
              </Form.Group>
              <Form.Group>
                <Form.Label>
                  Medidas de cintura y cadera en centímetros (cm)
                </Form.Label>
                <Form.Control
                  onChange={this.handleUserInput}
                  value={this.state.cintura_cadera}
                  name="cintura_cadera"
                  type="text"
                  placeholder="Tu respuesta"
                ></Form.Control>
              </Form.Group>
              <Form.Group>
                <Form.Label className="required">
                  Porcentaje de grasa corporal (si lo sabe actualizado) y cómo
                  fue medido?
                </Form.Label>
                <Form.Control
                  onChange={this.handleUserInput}
                  value={this.state.grasa_corporal}
                  isInvalid={
                    this.state.isclick && this.state.grasa_corporal === ''
                  }
                  name="grasa_corporal"
                  type="text"
                  placeholder="Tu respuesta"
                ></Form.Control>
              </Form.Group>
              <Form.Group>
                <Form.Label
                  className={
                    'required ' +
                    (this.state.isclick && this.state.foto === null
                      ? 'red'
                      : '')
                  }
                >
                  Adjunta una foto de frente con buena iluminación (En traje de baño)
                </Form.Label>
                <Form.Control
                  onChange={this.handleUserInput}
                  name="foto"
                  type="file"
                  style={{ border: 'none' }}
                  accept="image/png, image/jpeg"
                  placeholder="Tu respuesta"
                ></Form.Control>
              </Form.Group>
              <Form.Group>
                <Form.Label className="required">
                  Cantidad de agua que tomas al día (Indicar si en litros o
                  vasos de agua)
                </Form.Label>
                <Form.Control
                  onChange={this.handleUserInput}
                  value={this.state.cantidad_agua}
                  isInvalid={
                    this.state.isclick && this.state.cantidad_agua === ''
                  }
                  name="cantidad_agua"
                  type="text"
                  placeholder="Tu respuesta"
                ></Form.Control>
              </Form.Group>
              <Form.Group>
                <Form.Label className="required">
                  Fumas tabaco o eres fumador pasivo?
                </Form.Label>
                <Form.Check
                  custom
                  checked={this.state.tabaco === 'Si fumo tabaco'}
                  id="tabaco1"
                  onChange={this.handleUserInput}
                  value="Si fumo tabaco"
                  name="tabaco"
                  type="radio"
                  label={`Si fumo tabaco `}
                  isInvalid={this.state.isclick && this.state.tabaco === ''}
                />
                <Form.Check
                  custom
                  checked={
                    this.state.tabaco ===
                    'No fumo tabaco pero estoy constantemente en presencia de otros fumadores (Soy fumador pasivo)'
                  }
                  id="tabaco2"
                  onChange={this.handleUserInput}
                  value="No fumo tabaco pero estoy constantemente en presencia de otros fumadores (Soy fumador pasivo)"
                  name="tabaco"
                  type="radio"
                  label={`No fumo tabaco pero estoy constantemente en presencia de otros fumadores (Soy fumador pasivo) `}
                  isInvalid={this.state.isclick && this.state.tabaco === ''}
                />
                <Form.Check
                  custom
                  checked={
                    this.state.tabaco ===
                    'Ni fumo ni tengo constantemente fumadores a mi alrededor'
                  }
                  id="tabaco3"
                  onChange={this.handleUserInput}
                  value="Ni fumo ni tengo constantemente fumadores a mi alrededor"
                  name="tabaco"
                  type="radio"
                  label={`Ni fumo ni tengo constantemente fumadores a mi alrededor `}
                  isInvalid={this.state.isclick && this.state.tabaco === ''}
                />
              </Form.Group>
              <Form.Group>
                <Form.Label>Tomas café o té regularmente?</Form.Label>
                <Form.Check
                  custom
                  defaultChecked={this.state.cafe_te[0]}
                  onClick={() => this.handleCafeTeChange(0)}
                  name="cafe_te0"
                  id="cafe1"
                  type="checkbox"
                  label={`Té`}
                />
                <Form.Check
                  custom
                  defaultChecked={this.state.cafe_te[1]}
                  onClick={() => this.handleCafeTeChange(1)}
                  name="cafe_te1"
                  id="cafe2"
                  type="checkbox"
                  label={`Café`}
                />
                <Form.Check
                  custom
                  defaultChecked={this.state.cafe_te[2]}
                  onClick={() => this.handleCafeTeChange(2)}
                  name="cafe_te2"
                  id="cafe3"
                  type="checkbox"
                  label={`Ninguno`}
                />
              </Form.Group>
              <Form.Group>
                <Form.Label>
                  Cual es la frecuencia de evacuaciones? Cada cuanto vas al baño
                  ? Sufres de estreñimiento?
                </Form.Label>
                <Form.Control
                  onChange={this.handleUserInput}
                  value={this.state.evacuaciones}
                  name="evacuaciones"
                  type="text"
                  placeholder="Tu respuesta"
                ></Form.Control>
              </Form.Group>
              <Form.Group>
                <Form.Label>
                  Sientes ansiedad en algún momento del día ? Si la respuesta es
                  sí indicar a qué hora?
                </Form.Label>
                <Form.Control
                  onChange={this.handleUserInput}
                  value={this.state.ansiedad}
                  name="ansiedad"
                  type="text"
                  placeholder="Tu respuesta"
                ></Form.Control>
              </Form.Group>
              <Form.Group>
                <Form.Label className="required">
                  Describe un día típico para ti:
                </Form.Label>
                <Form.Check
                  custom
                  isInvalid={this.state.isclick && this.state.dia_tipico === ''}
                  checked={
                    this.state.dia_tipico ===
                    'En la oficina, trabajo sedentario frente a un ordenador'
                  }
                  id="diat1"
                  onChange={this.handleUserInput}
                  value="En la oficina, trabajo sedentario frente a un ordenador"
                  name="dia_tipico"
                  type="radio"
                  label={`En la oficina, trabajo sedentario frente a un ordenador. `}
                />
                <Form.Check
                  custom
                  isInvalid={this.state.isclick && this.state.dia_tipico === ''}
                  checked={
                    this.state.dia_tipico ===
                    'Paso la mayor parte de mi día de pie'
                  }
                  id="diat2"
                  onChange={this.handleUserInput}
                  value="Paso la mayor parte de mi día de pie"
                  name="dia_tipico"
                  type="radio"
                  label={`Paso la mayor parte de mi día de pie. `}
                />
                <Form.Check
                  custom
                  isInvalid={this.state.isclick && this.state.dia_tipico === ''}
                  checked={this.state.dia_tipico === 'Labor manual'}
                  id="diat3"
                  onChange={this.handleUserInput}
                  value="Labor manual"
                  name="dia_tipico"
                  type="radio"
                  label={`Labor manual. `}
                />
                <Form.Check
                  custom
                  isInvalid={this.state.isclick && this.state.dia_tipico === ''}
                  checked={
                    this.state.dia_tipico ===
                    'Paso la mayor parte del día en casa'
                  }
                  id="diat4"
                  onChange={this.handleUserInput}
                  value="Paso la mayor parte del día en casa"
                  name="dia_tipico"
                  type="radio"
                  label={`Paso la mayor parte del día en casa `}
                />
                <Form.Check
                  custom
                  isInvalid={this.state.isclick && this.state.dia_tipico === ''}
                  checked={
                    this.state.dia_tipico !==
                      'En la oficina, trabajo sedentario frente a un ordenador' &&
                    this.state.dia_tipico !==
                      'Paso la mayor parte de mi día de pie' &&
                    this.state.dia_tipico !== 'Labor manual' &&
                    this.state.dia_tipico !==
                      'Paso la mayor parte del día en casa'
                  }
                  id="diat5"
                  type="radio"
                  label={
                    <Form.Control
                      onChange={this.handleUserInput}
                      name="dia_tipico"
                      type="text"
                      placeholder="Otro"
                    ></Form.Control>
                  }
                />
              </Form.Group>
              <Form.Group>
                <Form.Label className="required">
                  ¿Has entrenado anteriormente?
                </Form.Label>
                <Form.Check
                  custom
                  isInvalid={
                    this.state.isclick &&
                    this.state.entrenamiento_anteriormente === ''
                  }
                  checked={this.state.entrenamiento_anteriormente === 'Si'}
                  id="entrenamiento_anteriormente2"
                  onChange={this.handleUserInput}
                  value="Si"
                  name="entrenamiento_anteriormente"
                  type="radio"
                  label={`Si `}
                />
                <Form.Check
                  custom
                  isInvalid={
                    this.state.isclick &&
                    this.state.entrenamiento_anteriormente === ''
                  }
                  checked={this.state.entrenamiento_anteriormente === 'No'}
                  id="entrenamiento_anteriormente1"
                  onChange={this.handleUserInput}
                  value="No"
                  name="entrenamiento_anteriormente"
                  type="radio"
                  label={`No `}
                />
              </Form.Group>
              <Form.Group>
                <Form.Label>
                  Si la anterior pregunta fue si, ¿Desde hace cuanto tiempo
                  entrenas (Indicar en meses o en años)?
                </Form.Label>
                <Form.Control
                  onChange={this.handleUserInput}
                  value={this.state.entrenamiento_desde_cuando}
                  name="entrenamiento_desde_cuando"
                  type="text"
                  placeholder="Tu respuesta"
                ></Form.Control>
              </Form.Group>
              <Form.Group>
                <Form.Label className="required">
                  Que tipo de entrenamiento realizas? Ser especifico
                </Form.Label>
                <Form.Control
                  isInvalid={
                    this.state.isclick && this.state.tipo_entrenamiento === ''
                  }
                  onChange={this.handleUserInput}
                  value={this.state.tipo_entrenamiento}
                  name="tipo_entrenamiento"
                  type="text"
                  placeholder="Tu respuesta"
                ></Form.Control>
              </Form.Group>
              <Form.Group>
                <Form.Label className="required">
                  Cual es tu experiencia (en años) con el entrenamiento de pesas
                  ?
                </Form.Label>
                <Form.Check
                  custom
                  isInvalid={
                    this.state.isclick && this.state.experiencia_pesas === ''
                  }
                  checked={this.state.experiencia_pesas === 'Ninguna'}
                  id="experiencia_pesas1"
                  onChange={this.handleUserInput}
                  value="Ninguna"
                  name="experiencia_pesas"
                  type="radio"
                  label={`Ninguna `}
                />
                <Form.Check
                  custom
                  isInvalid={
                    this.state.isclick && this.state.experiencia_pesas === ''
                  }
                  checked={this.state.experiencia_pesas === 'Menos de 5 años'}
                  id="experiencia_pesas2"
                  onChange={this.handleUserInput}
                  value="Menos de 5 años"
                  name="experiencia_pesas"
                  type="radio"
                  label={`Menos de 5 años `}
                />
                <Form.Check
                  custom
                  isInvalid={
                    this.state.isclick && this.state.experiencia_pesas === ''
                  }
                  checked={this.state.experiencia_pesas === 'Más de 5 años'}
                  id="experiencia_pesas3"
                  onChange={this.handleUserInput}
                  value="Más de 5 años"
                  name="experiencia_pesas"
                  type="radio"
                  label={`Más de 5 años `}
                />
              </Form.Group>
              <Form.Group>
                <Form.Label className="required">
                  A qué hora del día entrenas normalmente?
                </Form.Label>
                <Form.Control
                  isInvalid={
                    this.state.isclick && this.state.hora_entrenamiento === ''
                  }
                  onChange={this.handleUserInput}
                  value={this.state.hora_entrenamiento}
                  name="hora_entrenamiento"
                  type="text"
                  placeholder="Tu respuesta"
                ></Form.Control>
              </Form.Group>
              <Form.Group>
                <Form.Label className="required">
                  ¿Cuantos días a la semana entrenas o dispones para entrenar?
                  Sé realista
                </Form.Label>
                <Form.Control
                  isInvalid={
                    this.state.isclick && this.state.dias_semana_entrena === ''
                  }
                  onChange={this.handleUserInput}
                  value={this.state.dias_semana_entrena}
                  name="dias_semana_entrena"
                  as="select"
                >
                  <option>1 día</option>
                  <option>2 días</option>
                  <option>3 días</option>
                  <option>4 días</option>
                  <option>5 días</option>
                  <option>6 días</option>
                  <option>7 días</option>
                </Form.Control>
              </Form.Group>
              <Form.Group>
                <Form.Label className="required">
                  ¿De cuanto tiempo dispones al día para entrenar? Sé realista
                </Form.Label>
                <Form.Control
                  isInvalid={
                    this.state.isclick && this.state.tiempo_para_entrenar === ''
                  }
                  onChange={this.handleUserInput}
                  value={this.state.tiempo_para_entrenar}
                  name="tiempo_para_entrenar"
                  as="select"
                >
                  <option>15 minutos</option>
                  <option>30 minutos</option>
                  <option>45 minutos</option>
                  <option>1 hora</option>
                  <option>1 hora y media</option>
                  <option>2 horas</option>
                  <option>más de 2 horas</option>
                </Form.Control>
              </Form.Group>
              <Form.Group>
                <Form.Label className="required">
                  ¿Objetivos y expectativas con nuestra asesoría?
                </Form.Label>
                <Form.Label style={{ fontSize: '10px' }}>
                  Cual es el objetivo que te planteas lograr con nuestra
                  asesoría
                </Form.Label>
                <Form.Check
                  isInvalid={
                    this.state.isclick &&
                    this.state.objetivos_espectativas === ''
                  }
                  custom
                  checked={
                    this.state.objetivos_espectativas === 'Definición muscular'
                  }
                  id="objetivos_espectativas1"
                  onChange={this.handleUserInput}
                  value="Definición muscular"
                  name="objetivos_espectativas"
                  type="radio"
                  label={`Definición muscular `}
                />
                <Form.Check
                  isInvalid={
                    this.state.isclick &&
                    this.state.objetivos_espectativas === ''
                  }
                  custom
                  checked={
                    this.state.objetivos_espectativas ===
                    'Aumento de masa muscular'
                  }
                  id="objetivos_espectativas2"
                  onChange={this.handleUserInput}
                  value="Aumento de masa muscular"
                  name="objetivos_espectativas"
                  type="radio"
                  label={`Aumento de masa muscular `}
                />
                <Form.Check
                  isInvalid={
                    this.state.isclick &&
                    this.state.objetivos_espectativas === ''
                  }
                  custom
                  checked={
                    this.state.objetivos_espectativas ===
                    'Mantenimiento de la forma física'
                  }
                  id="objetivos_espectativas3"
                  onChange={this.handleUserInput}
                  value="Mantenimiento de la forma física"
                  name="objetivos_espectativas"
                  type="radio"
                  label={`Mantenimiento de la forma física `}
                />
                <Form.Check
                  isInvalid={
                    this.state.isclick &&
                    this.state.objetivos_espectativas === ''
                  }
                  custom
                  checked={
                    this.state.objetivos_espectativas !==
                      'Definición muscular' &&
                    this.state.objetivos_espectativas !==
                      'Aumento de masa muscular' &&
                    this.state.objetivos_espectativas !==
                      'Mantenimiento de la forma física'
                  }
                  id="objetivos_espectativas4"
                  type="radio"
                  label={
                    <Form.Control
                      onChange={this.handleUserInput}
                      name="objetivos_espectativas"
                      type="text"
                      placeholder="Otro"
                    ></Form.Control>
                  }
                />
              </Form.Group>
              <Form.Group>
                <Form.Label className="required">
                  Explicar con detalle cual es el objetivo que quieres lograr
                  con nuestra asesoría
                </Form.Label>
                <Form.Control
                  isInvalid={
                    this.state.isclick && this.state.objetivo_detallado === ''
                  }
                  onChange={this.handleUserInput}
                  value={this.state.objetivo_detallado}
                  name="objetivo_detallado"
                  type="text"
                  placeholder="Tu respuesta"
                ></Form.Control>
              </Form.Group>
              <Form.Group>
                <Form.Label className="required">
                  Cuantas comida haces al día normalmente y en que horarios?
                </Form.Label>
                <Form.Control
                  isInvalid={
                    this.state.isclick && this.state.comidas_dia === ''
                  }
                  onChange={this.handleUserInput}
                  value={this.state.comidas_dia}
                  name="comidas_dia"
                  type="text"
                  placeholder="Tu respuesta"
                ></Form.Control>
              </Form.Group>
              <Form.Group>
                <Form.Label>
                  Qué Frutas y vegetales consumes normalmente?
                </Form.Label>
                <Form.Control
                  onChange={this.handleUserInput}
                  value={this.state.frutas_vegetales_consume}
                  name="frutas_vegetales_consume"
                  type="text"
                  placeholder="Tu respuesta"
                ></Form.Control>
              </Form.Group>
              <Form.Group>
                <Form.Label>
                  Qué alimentos nunca consumes porque no te gustan, te dan
                  alergia o intolerancia?
                </Form.Label>
                <Form.Control
                  onChange={this.handleUserInput}
                  value={this.state.alimento_no_consume}
                  name="alimento_no_consume"
                  type="text"
                  placeholder="Tu respuesta"
                ></Form.Control>
              </Form.Group>
            </div>
          </div>
          <h4>
            ¿Qué comiste el día de ayer y en qué cantidades aproximadamente
            (tazas/cucharadas/peso)? (Responder esta pregunta es obligatoria)
          </h4>
          <div className="row">
            <div className="col-lg-8">
              <label>
                Rellene a continuación con las comidas en cada una de las
                opciones agregando también la hora aproximada en que realizo la
                comida
              </label>
              <Form.Group>
                <Form.Label className="required">Desayuno de ayer</Form.Label>
                <Form.Control
                  isInvalid={this.state.isclick && this.state.desayuno === ''}
                  onChange={this.handleUserInput}
                  value={this.state.desayuno}
                  name="desayuno"
                  type="text"
                  placeholder="Tu respuesta"
                ></Form.Control>
              </Form.Group>
              <Form.Group>
                <Form.Label>
                  Merienda de media mañana de ayer (Si la hubo)
                </Form.Label>
                <Form.Control
                  onChange={this.handleUserInput}
                  value={this.state.merienda_media_mañana}
                  name="merienda_media_mañana"
                  type="text"
                  placeholder="Tu respuesta"
                ></Form.Control>
              </Form.Group>
              <Form.Group>
                <Form.Label className="required">
                  Almuerzo o comida de ayer
                </Form.Label>
                <Form.Control
                  isInvalid={this.state.isclick && this.state.almuerzo === ''}
                  onChange={this.handleUserInput}
                  value={this.state.almuerzo}
                  name="almuerzo"
                  type="text"
                  placeholder="Tu respuesta"
                ></Form.Control>
              </Form.Group>
              <Form.Group>
                <Form.Label>
                  Merienda de media tarde de ayer (Si la hubo)
                </Form.Label>
                <Form.Control
                  onChange={this.handleUserInput}
                  value={this.state.merienda_media_tarde}
                  name="merienda_media_tarde"
                  type="text"
                  placeholder="Tu respuesta"
                ></Form.Control>
              </Form.Group>
              <Form.Group>
                <Form.Label className="required">Cena de ayer</Form.Label>
                <Form.Control
                  isInvalid={this.state.isclick && this.state.cena === ''}
                  onChange={this.handleUserInput}
                  value={this.state.cena}
                  name="cena"
                  type="text"
                  placeholder="Tu respuesta"
                ></Form.Control>
              </Form.Group>
              <Form.Group>
                <Form.Label>
                  Merienda antes de dormir de ayer (Si la hubo)
                </Form.Label>
                <Form.Control
                  onChange={this.handleUserInput}
                  value={this.state.merienda_antes_dormir}
                  name="merienda_antes_dormir"
                  type="text"
                  placeholder="Tu respuesta"
                ></Form.Control>
              </Form.Group>
              <Form.Group>
                <Form.Label className="required">
                  ¿Has tomado o tomas suplementos?
                </Form.Label>
                <Form.Check
                  custom
                  isInvalid={
                    this.state.isclick && this.state.tomas_suplementos === ''
                  }
                  checked={
                    this.state.tomas_suplementos ===
                    'Si tomo actualmente suplemento(s)'
                  }
                  id="tomas_suplementos1"
                  onChange={this.handleUserInput}
                  value="Si tomo actualmente suplemento(s)"
                  name="tomas_suplementos"
                  type="radio"
                  label={`Si tomo actualmente suplemento(s) `}
                />
                <Form.Check
                  custom
                  isInvalid={
                    this.state.isclick && this.state.tomas_suplementos === ''
                  }
                  checked={
                    this.state.tomas_suplementos ===
                    'He tomado alguno(s) en el pasado pero actualmente no tomo ninguno'
                  }
                  id="tomas_suplementos2"
                  onChange={this.handleUserInput}
                  value="He tomado alguno(s) en el pasado pero actualmente no tomo ninguno"
                  name="tomas_suplementos"
                  type="radio"
                  label={`He tomado alguno(s) en el pasado pero actualmente no tomo ninguno `}
                />
                <Form.Check
                  custom
                  isInvalid={
                    this.state.isclick && this.state.tomas_suplementos === ''
                  }
                  checked={
                    this.state.tomas_suplementos ===
                    'No he tomado nunca ningún tipo de suplemento'
                  }
                  id="tomas_suplementos3"
                  onChange={this.handleUserInput}
                  value="No he tomado nunca ningún tipo de suplemento"
                  name="tomas_suplementos"
                  type="radio"
                  label={`No he tomado nunca ningún tipo de suplemento `}
                />
              </Form.Group>
              <Form.Group>
                <Form.Label>
                  Si la anterior respuesta fue si, dime cuales suplementos has
                  tomado o tomas actualmente?
                </Form.Label>
                <Form.Control
                  onChange={this.handleUserInput}
                  value={this.state.suplementos_que_toma}
                  name="suplementos_que_toma"
                  type="text"
                  placeholder="Tu respuesta"
                ></Form.Control>
              </Form.Group>
              <Form.Group>
                <Form.Label className="required">
                  ¿Tienes alguna Alergias / Intolerancias / Enfermedades /
                  Medicación? Especificar.
                </Form.Label>
                <Form.Control
                  isInvalid={this.state.isclick && this.state.alergias === ''}
                  onChange={this.handleUserInput}
                  value={this.state.alergias}
                  name="alergias"
                  type="text"
                  placeholder="Tu respuesta"
                ></Form.Control>
              </Form.Group>
              <Form.Group>
                <Form.Label className="required">
                  Le ha dicho su médico alguna vez que padece una enfermedad
                  cardiaca y que sólo debe hacer aquella actividad física que le
                  aconseje un médico?
                </Form.Label>
                <Form.Check
                  custom
                  isInvalid={
                    this.state.isclick && this.state.enfermedad_cardiaca === ''
                  }
                  checked={this.state.enfermedad_cardiaca === 'Si'}
                  id="enfermedad_cardiaca1"
                  onChange={this.handleUserInput}
                  value="Si"
                  name="enfermedad_cardiaca"
                  type="radio"
                  label={`Si `}
                />
                <Form.Check
                  custom
                  isInvalid={
                    this.state.isclick && this.state.enfermedad_cardiaca === ''
                  }
                  checked={this.state.enfermedad_cardiaca === 'No'}
                  id="enfermedad_cardiaca2"
                  onChange={this.handleUserInput}
                  value="No"
                  name="enfermedad_cardiaca"
                  type="radio"
                  label={`No `}
                />
              </Form.Group>
              <Form.Group>
                <Form.Label className="required">
                  Tiene dolor en el pecho cuando hace actividad física?
                </Form.Label>
                <Form.Check
                  custom
                  isInvalid={
                    this.state.isclick && this.state.dolor_de_pecho === ''
                  }
                  checked={this.state.dolor_de_pecho === 'Si'}
                  id="dolor_de_pecho1"
                  onChange={this.handleUserInput}
                  value="Si"
                  name="dolor_de_pecho"
                  type="radio"
                  label={`Si `}
                />
                <Form.Check
                  custom
                  isInvalid={
                    this.state.isclick && this.state.dolor_de_pecho === ''
                  }
                  checked={this.state.dolor_de_pecho === 'No'}
                  id="dolor_de_pecho2"
                  onChange={this.handleUserInput}
                  value="No"
                  name="dolor_de_pecho"
                  type="radio"
                  label={`No `}
                />
              </Form.Group>
              <Form.Group>
                <Form.Label className="required">
                  En el último mes, ¿ha tenido dolor en el pecho cuando no hacía
                  actividad física?
                </Form.Label>
                <Form.Check
                  custom
                  isInvalid={
                    this.state.isclick &&
                    this.state.dolor_de_pecho_sin_ejercicio === ''
                  }
                  checked={this.state.dolor_de_pecho_sin_ejercicio === 'Si'}
                  id="dolor_de_pecho_sin_ejercicio1"
                  onChange={this.handleUserInput}
                  value="Si"
                  name="dolor_de_pecho_sin_ejercicio"
                  type="radio"
                  label={`Si `}
                />
                <Form.Check
                  custom
                  isInvalid={
                    this.state.isclick &&
                    this.state.dolor_de_pecho_sin_ejercicio === ''
                  }
                  checked={this.state.dolor_de_pecho_sin_ejercicio === 'No'}
                  id="dolor_de_pecho_sin_ejercicio2"
                  onChange={this.handleUserInput}
                  value="No"
                  name="dolor_de_pecho_sin_ejercicio"
                  type="radio"
                  label={`No `}
                />
              </Form.Group>
              <Form.Group>
                <Form.Label className="required">
                  Pierde el equilibrio debido a mareos o se ha desmayado alguna
                  vez?
                </Form.Label>
                <Form.Check
                  custom
                  isInvalid={
                    this.state.isclick && this.state.pierde_equilibrio === ''
                  }
                  checked={this.state.pierde_equilibrio === 'Si'}
                  id="pierde_equilibrio1"
                  onChange={this.handleUserInput}
                  value="Si"
                  name="pierde_equilibrio"
                  type="radio"
                  label={`Si `}
                />
                <Form.Check
                  custom
                  isInvalid={
                    this.state.isclick && this.state.pierde_equilibrio === ''
                  }
                  checked={this.state.pierde_equilibrio === 'No'}
                  id="pierde_equilibrio2"
                  onChange={this.handleUserInput}
                  value="No"
                  name="pierde_equilibrio"
                  type="radio"
                  label={`No `}
                />
              </Form.Group>
              <Form.Group>
                <Form.Label className="required">
                  Tiene problemas en huesos o articulaciones (por ejemplo,
                  espalda, rodilla o cadera) que puedan empeorar si aumenta la
                  actividad física?
                </Form.Label>
                <Form.Check
                  custom
                  isInvalid={
                    this.state.isclick && this.state.problemas_huesos === ''
                  }
                  checked={this.state.problemas_huesos === 'Si'}
                  id="problemas_huesos1"
                  onChange={this.handleUserInput}
                  value="Si"
                  name="problemas_huesos"
                  type="radio"
                  label={`Si `}
                />
                <Form.Check
                  custom
                  isInvalid={
                    this.state.isclick && this.state.problemas_huesos === ''
                  }
                  checked={this.state.problemas_huesos === 'No'}
                  id="problemas_huesos2"
                  onChange={this.handleUserInput}
                  value="No"
                  name="problemas_huesos"
                  type="radio"
                  label={`No `}
                />
              </Form.Group>
              <Form.Group>
                <Form.Label className="required">
                  Le receta su médico algún medicamento para la tensión arterial
                  o un problema cardíaco?
                </Form.Label>
                <Form.Check
                  custom
                  isInvalid={
                    this.state.isclick && this.state.medicamento_tension === ''
                  }
                  checked={this.state.medicamento_tension === 'Si'}
                  id="medicamento_tension1"
                  onChange={this.handleUserInput}
                  value="Si"
                  name="medicamento_tension"
                  type="radio"
                  label={`Si `}
                />
                <Form.Check
                  custom
                  isInvalid={
                    this.state.isclick && this.state.medicamento_tension === ''
                  }
                  checked={this.state.medicamento_tension === 'No'}
                  id="medicamento_tension2"
                  onChange={this.handleUserInput}
                  value="No"
                  name="medicamento_tension"
                  type="radio"
                  label={`No `}
                />
              </Form.Group>
              <Form.Group>
                <Form.Label className="required">
                  Conoce alguna razón por la cual no debería realizar actividad
                  física?
                </Form.Label>
                <Form.Check
                  custom
                  isInvalid={
                    this.state.isclick && this.state.razon_no_ejercicio === ''
                  }
                  checked={this.state.razon_no_ejercicio === 'Si'}
                  id="razon_no_ejercicio1"
                  onChange={this.handleUserInput}
                  value="Si"
                  name="razon_no_ejercicio"
                  type="radio"
                  label={`Si `}
                />
                <Form.Check
                  custom
                  isInvalid={
                    this.state.isclick && this.state.razon_no_ejercicio === ''
                  }
                  checked={this.state.razon_no_ejercicio === 'No'}
                  id="razon_no_ejercicio2"
                  onChange={this.handleUserInput}
                  value="No"
                  name="razon_no_ejercicio"
                  type="radio"
                  label={`No `}
                />
              </Form.Group>
              <Form.Group>
                <Form.Label className="required">
                  ¿Tienes algún impedimento físico o has sufrido alguna lesión
                  que te limite o no te permita realizar ejercicio físico?
                </Form.Label>
                <Form.Control
                  isInvalid={
                    this.state.isclick && this.state.impedimento === ''
                  }
                  onChange={this.handleUserInput}
                  value={this.state.impedimento}
                  name="impedimento"
                  type="text"
                  placeholder="Tu respuesta"
                ></Form.Control>
              </Form.Group>
              <Form.Group>
                <Form.Label>
                  Comentarios adicionales que desees agregar.
                </Form.Label>
                <Form.Control
                  onChange={this.handleUserInput}
                  value={this.state.comentarios}
                  name="comentarios"
                  type="text"
                  placeholder="Tu respuesta"
                ></Form.Control>
              </Form.Group>
              <Form.Group>
                <Form.Label className="required">
                  Déjanos tu número de Whatsapp (con código de tu país)
                </Form.Label>
                <Form.Label style={{ fontSize: '10px' }}>
                  Lo necesitaremos en caso de tener que aclarar alguna duda
                </Form.Label>
                <Form.Control
                  isInvalid={this.state.isclick && this.state.whatsapp === ''}
                  onChange={this.handleUserInput}
                  value={this.state.whatsapp}
                  name="whatsapp"
                  type="number"
                  placeholder="Tu respuesta"
                ></Form.Control>
              </Form.Group>
            </div>
          </div>
          <div style={{display:'flex',alignItems:'center'}}>
            <Button onClick={this.sendDataToEmail} disabled={this.state.onLoad}>Enviar formulario</Button>
            {this.state.onLoad ? <img style={{width: '35px', marginLeft: '20px'}} src={dots} alt="loader"/>:null}
          </div>
        </Form>
      </div>
    )
  }
}

const mapStateToProps = state => ({
  planValue: state.planValue,
  totalValue: state.totalValue,
  dietaValue: state.dietaValue,
  donationValue: state.donationValue,
})
const mapDispatchToProps = dispatch => ({
  changePage() {
    dispatch({
      type: 'CHANGE_PAGE',
      page: 11,
    })
  },
})
export default connect(mapStateToProps, mapDispatchToProps)(FormPage)
