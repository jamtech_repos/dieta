import React, { Component } from 'react';
import { connect } from 'react-redux';
import soymilk from './svg/001-soy-milk.svg';
import milk from './svg/002-milk-products.svg';
import eggs from './svg/003-eggs.svg';
import walnut from './svg/004-walnut.svg';
import gluten from './svg/005-gluten.svg';
import noallergies from './svg/no-stopping.svg';
import './allergies.css';

class Allergies extends Component {
  render() {
    return (
      <div className='container'>
        <div className='row'>
          <div className='section-text__allergies col-md-12 col-lg-6'>
            Alergias y/o intolerancias
          </div>
          <div className='options col-md-12 col-lg-6'>
            <p>Elige tus Alergias y/o intolerancias y haz click en siguiente</p>
            <div className='row'>
              <div
                onClick={() => this.props.selectAllergies(0)}
                className={
                  'elementicon col-4 ' +
                  (this.props.alergias[0] ? 'option-active' : null)
                }>
                <img src={soymilk} alt='Soja' />
                <label>Soja</label>
              </div>
              <div
                onClick={() => this.props.selectAllergies(1)}
                className={
                  'elementicon col-4 ' +
                  (this.props.alergias[1] ? 'option-active' : null)
                }>
                <div style={{ display: 'contents' }}>
                  <img src={milk} alt='Lácteos' />
                </div>
                <label>Lácteos</label>
              </div>
              <div
                onClick={() => this.props.selectAllergies(2)}
                className={
                  'elementicon col-4 ' +
                  (this.props.alergias[2] ? 'option-active' : null)
                }>
                <div style={{ display: 'contents' }}>
                  <img src={eggs} alt='Huevos' />
                </div>
                <label>Huevos</label>
              </div>
              <div
                onClick={() => this.props.selectAllergies(3)}
                className={
                  'elementicon col-4 ' +
                  (this.props.alergias[3] ? 'option-active' : null)
                }>
                <div style={{ display: 'contents' }}>
                  <img src={walnut} alt='Nueces' />
                </div>
                <label>Nueces</label>
              </div>
              <div
                onClick={() => this.props.selectAllergies(4)}
                className={
                  'elementicon col-4 ' +
                  (this.props.alergias[4] ? 'option-active' : null)
                }>
                <div style={{ display: 'contents' }}>
                  <img src={gluten} alt='Gluten' />
                </div>
                <label>Gluten</label>
              </div>
              <div
                onClick={() => this.props.selectAllergies(5)}
                className={
                  'elementicon col-4 ' +
                  (this.props.alergias[5] ? 'option-active' : null)
                }>
                <div style={{ display: 'contents' }}>
                  <img src={noallergies} alt='Ninguna' />
                </div>
                <label>Ninguna</label>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}
const mapStateToProps = state => ({
  alergias: state.alergias
});
const mapDispatchToProps = dispatch => ({
  selectAllergies(option) {
    dispatch({
      type: 'SELECT_ALLERGIES',
      optionallergies: option
    });
  }
});
export default connect(mapStateToProps, mapDispatchToProps)(Allergies);
