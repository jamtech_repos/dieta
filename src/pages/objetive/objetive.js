import React, { Component } from 'react';
import { connect } from 'react-redux';
import aumento from './svg/001-muscles.svg';
import mantenimiento from './svg/003-yoga.svg';
import quemargrasa from './svg/002-waist.svg';
import './objetives.css';
class Objetives extends Component {
  render() {
    return (
      <div className='container'>
        <div className='row'>
          <div className='section-text col-md-12 col-lg-6'>Tu objetivo</div>
          <div className='options col-md-12 col-lg-6'>
            <p>Elige tu objetivo y haz click en siguiente</p>
            <div className='row'>
              <div
                onClick={() => this.props.selectObjetive(1)}
                className={
                  'elementicon col-4 ' +
                  (this.props.objetivo === 1 ? 'option-active' : null)
                }>
                <div style={{ display: 'contents' }}>
                  <img src={aumento} alt='' />
                </div>
                <label>Aumentar masa muscular</label>
              </div>
              <div
                onClick={() => this.props.selectObjetive(2)}
                className={
                  'elementicon col-4 ' +
                  (this.props.objetivo === 2 ? 'option-active' : null)
                }>
                <div style={{ display: 'contents' }}>
                  <img src={mantenimiento} alt='' />
                </div>
                <label>Mantenimiento</label>
              </div>
              <div
                onClick={() => this.props.selectObjetive(3)}
                className={
                  'elementicon col-4 ' +
                  (this.props.objetivo === 3 ? 'option-active' : null)
                }>
                <div style={{ display: 'contents' }}>
                  <img src={quemargrasa} alt='' />
                </div>
                <label>Perder grasa corporal</label>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}
const mapStateToProps = state => ({
  objetivo: state.objetivo
});
const mapDispatchToProps = dispatch => ({
  selectObjetive(option) {
    dispatch({
      type: 'SELECT_OBJETIVE',
      objetivo: option
    });
  }
});
export default connect(mapStateToProps, mapDispatchToProps)(Objetives);
