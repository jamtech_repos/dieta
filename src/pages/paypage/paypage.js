import React, { Component } from 'react'
import { connect } from 'react-redux'
import { PayPalButton } from 'react-paypal-button'
import './paypage.css'
class PayPage extends Component {
  constructor(props) {
    super(props)
    this.state = {
      donation: false,
      ejercicio: false,
    }
    this.changeInputEjercicio = this.changeInputEjercicio.bind(this)
    this.changeInputDonation = this.changeInputDonation.bind(this)
  }

  changeInputEjercicio(event) {
    var e = event.target.checked
    this.setState({ ejercicio: e })
    var value =
      this.props.planValue +
      (this.state.donation ? this.props.donationValue : 0) +
      (e ? this.props.dietaValue : 0)
    this.props.changeValue(value)
  }
  changeInputDonation(event) {
    var e = event.target.checked
    this.setState({ donation: e })
    var value =
      this.props.planValue +
      (e ? this.props.donationValue : 0) +
      (this.state.ejercicio ? this.props.dietaValue : 0)
    this.props.changeValue(value)
  }
  render() {
    const paypalOptions = {
      clientId:
        'AXnAVfMHEHTI3Xr8EVONNVChQWHvCbTskBjdIeKIWapL_hdHTT4DMA5WySVZNPRRG42KZTjNF3mXIou-',
      /* Test */
      /* clientId:
        'Acv8KQRBd3GRFIf4NSUP_wYw_VZu55tAx5GBPh3y89eWgdAoFjLiik21NL7rrOdyqOM56IOQh24j_URz', */
      intent: 'capture',
    }

    const buttonStyles = {
      layout: 'horizontal',
      shape: 'rect',
      tagline: 'false',
    }
    return (
      <div className="container">
        <div className="content-plan">
          <table>
            <tbody>
              <tr>
                <td className="titleplan">
                  <h4>Tu dieta personalizada...</h4>
                </td>
                <td className="planvalue">
                  <h4>{this.props.planValue} $</h4>
                </td>
              </tr>
            </tbody>
          </table>
          <h5 className="extra"><b>OFERTA ÚNICA:</b></h5>
          <div>
            <table>
              <tbody>
                <tr>
                  <td className="td-first">
                    <input
                      defaultChecked={this.state.ejercicio}
                      onChange={this.changeInputEjercicio}
                      className="inp-cbx"
                      id="cbx_ejercice"
                      type="checkbox"
                      style={{ display: 'none' }}
                    />
                    <label
                      style={{ width: '100%' }}
                      className="cbx"
                      htmlFor="cbx_ejercice"
                    >
                      <span>
                        <svg
                          width="15px"
                          version="1.1"
                          id="Capa_2"
                          x="0px"
                          y="0px"
                          viewBox="0 0 491.86 491.86"
                          xmlSpace="preserve"
                        >
                          <path d="M465.167,211.614H280.245V26.691c0-8.424-11.439-26.69-34.316-26.69s-34.316,18.267-34.316,26.69v184.924H26.69    C18.267,211.614,0,223.053,0,245.929s18.267,34.316,26.69,34.316h184.924v184.924c0,8.422,11.438,26.69,34.316,26.69    s34.316-18.268,34.316-26.69V280.245H465.17c8.422,0,26.69-11.438,26.69-34.316S473.59,211.614,465.167,211.614z"></path>
                        </svg>
                      </span>
                      <span
                        className="check"
                        style={{
                          lineHeight: '19px',
                          color: this.state.ejercicio ? '#4db2ec' : null,
                        }}
                      >
                        AÑADE Tu Entrenamiento personalizado (en casa o en gym)
                        <b> Oferta disponible solo AQUÍ y AHORA </b>. 
                      </span>
                    </label>
                  </td>
                  <td
                    className="td-end"
                    style={{
                      textAlign: 'end',
                      fontWeight: '700',
                      verticalAlign: 'middle',
                      paddingTop: '7px',
                      color: this.state.ejercicio ? '#4db2ec' : null,
                    }}
                  >
                    {this.props.dietaValue} $
                  </td>
                </tr>
                {/*<tr>
                  <td>
                    <input
                      defaultChecked={this.state.donation}
                      onClick={this.changeInputDonation}
                      className="inp-cbx"
                      id="cbx_donation"
                      type="checkbox"
                      style={{ display: 'none' }}
                    />
                    <label
                      style={{
                        width: '100%',
                        marginTop: '10px',
                        paddingTop: '10px',
                      }}
                      className="cbx"
                      htmlFor="cbx_donation"
                    >
                      <span>
                        <svg
                          width="15px"
                          version="1.1"
                          id="Capa_1"
                          x="0px"
                          y="0px"
                          viewBox="0 0 491.86 491.86"
                          xmlSpace="preserve"
                        >
                          <path d="M465.167,211.614H280.245V26.691c0-8.424-11.439-26.69-34.316-26.69s-34.316,18.267-34.316,26.69v184.924H26.69    C18.267,211.614,0,223.053,0,245.929s18.267,34.316,26.69,34.316h184.924v184.924c0,8.422,11.438,26.69,34.316,26.69    s34.316-18.268,34.316-26.69V280.245H465.17c8.422,0,26.69-11.438,26.69-34.316S473.59,211.614,465.167,211.614z"></path>
                        </svg>
                      </span>
                      <span
                        className="check"
                        style={{
                          lineHeight: '19px',
                          color: this.state.donation ? '#4db2ec' : null,
                        }}
                      >
                        Regala una comida a niños en desnutrición en Venezuela
                      </span>
                    </label>
                  </td>
                  <td
                    className="td-end"
                    style={{
                      textAlign: 'end',
                      fontWeight: '700',
                      verticalAlign: 'middle',
                      paddingTop: '10px',
                      color: this.state.donation ? '#4db2ec' : null,
                    }}
                  >
                    {this.props.donationValue} $
                  </td>
                </tr>*/}
              </tbody>
            </table>
          </div>
        </div>
        <div className="total">
          <div className="title">Total</div>
          <div className="ammount">{this.props.totalValue} $</div>
          <PayPalButton
            key={this.props.totalValue}
            paypalOptions={paypalOptions}
            buttonStyles={buttonStyles}
            amount={this.props.totalValue}
            onPaymentSuccess={response => this.props.changePage(response)}
          />
        </div>
      </div>
    )
  }
}

const mapStateToProps = state => ({
  planValue: state.planValue,
  totalValue: state.totalValue,
  dietaValue: state.dietaValue,
  donationValue: state.donationValue,
})
const mapDispatchToProps = dispatch => ({
  changeValue(value) {
    dispatch({
      type: 'CHAGE_PAYVALUE',
      value: value,
    })
  },
  changePage(response) {
    dispatch({
      type: 'CHANGE_PAGE',
      page: 10,
    })
  },
})
export default connect(mapStateToProps, mapDispatchToProps)(PayPage)
