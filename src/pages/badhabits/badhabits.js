import React, { Component } from 'react';
import { connect } from 'react-redux';
import nosleep from './svg/001-slumber.svg';
import comertarde from './svg/002-cutlery.svg';
import muchasal from './svg/003-salt.svg';
import muchodulce from './svg/004-toffee.svg';
import tomargaseosas from './svg/005-can.svg';
import ninguno from '../allergies/svg/no-stopping.svg';
import './badhabits.css';

class BadHabits extends Component {
  render() {
    return (
      <div className='container'>
        <div className='row'>
          <div className='section-textbadhabits col-md-12 col-lg-6'>
            Tus malos hábitos
          </div>
          <div className='options col-md-12 col-lg-6'>
            <p>Selecciona tus malos hábitos y haz click en siguiente</p>
            <div className='row'>
              <div
                onClick={() => this.props.selectOptionBadHabit(0)}
                className={
                  'elementicon col-4 ' +
                  (this.props.maloshabitos[0] ? 'option-active' : null)
                }>
                <div style={{ display: 'contents' }}>
                  <img src={nosleep} alt='No dormir lo suficiente' />
                </div>
                <label>No dormir lo suficiente</label>
              </div>
              <div
                onClick={() => this.props.selectOptionBadHabit(1)}
                className={
                  'elementicon col-4 ' +
                  (this.props.maloshabitos[1] ? 'option-active' : null)
                }>
                <div style={{ display: 'contents' }}>
                  <img src={comertarde} alt='Comer muy tarde' />
                </div>
                <label>Comer muy tarde</label>
              </div>
              <div
                onClick={() => this.props.selectOptionBadHabit(2)}
                className={
                  'elementicon col-4 ' +
                  (this.props.maloshabitos[2] ? 'option-active' : null)
                }>
                <div style={{ display: 'contents' }}>
                  <img src={muchasal} alt='Mucha sal en las comidas' />
                </div>
                <label>Mucha sal en las comidas</label>
              </div>
              <div
                onClick={() => this.props.selectOptionBadHabit(3)}
                className={
                  'elementicon col-4 ' +
                  (this.props.maloshabitos[3] ? 'option-active' : null)
                }>
                <div style={{ display: 'contents' }}>
                  <img src={muchodulce} alt='Mucho dulce y bollería' />
                </div>
                <label>Mucho dulce y bollería</label>
              </div>
              <div
                onClick={() => this.props.selectOptionBadHabit(4)}
                className={
                  'elementicon col-4 ' +
                  (this.props.maloshabitos[4] ? 'option-active' : null)
                }>
                <div style={{ display: 'contents' }}>
                  <img src={tomargaseosas} alt='Tomar gaseosas' />
                </div>
                <label>Tomar gaseosas</label>
              </div>
              <div
                onClick={() => this.props.selectOptionBadHabit(5)}
                className={
                  'elementicon col-4 ' +
                  (this.props.maloshabitos[5] ? 'option-active' : null)
                }>
                <div style={{ display: 'contents' }}>
                  <img src={ninguno} alt='Ninguno' />
                </div>
                <label>Ninguno</label>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}
const mapStateToProps = state => ({
  maloshabitos: state.maloshabitos
});
const mapDispatchToProps = dispatch => ({
  selectOptionBadHabit(option) {
    dispatch({
      type: 'SELECT_BADHABITS',
      optionbadhabits: option
    });
  }
});
export default connect(mapStateToProps, mapDispatchToProps)(BadHabits);
