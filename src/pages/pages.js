import React, { Component } from 'react'
import Intro from '../components/intro/intro'
import Stepper from '../components/stepper/stepper'
import { connect } from 'react-redux'
import Arrow from '../components/arrow/arrow'
import Sex from './sex/sex'
import PhysicalActivity from './physical_activity/physical_activity'
import Meats from './meats/meats'
import Allergies from './allergies/allergies'
import BadHabits from './badhabits/badhabits'
import Objetives from './objetive/objetive'
import Measurements from './measurements/measurements'
import Results from './results/results'
import Paypage from './paypage/paypage'
import FormPage from './form/form'
import Endpage from './endpage/endpage'
import moment from 'moment'
import './page.css'

class Page extends Component {
  componentDidMount() {
    var dateEnd = localStorage.getItem('venc')
    var dataCalc = localStorage.getItem('dataCalc')
    const dateCalc = localStorage.getItem('dateCalc')
    if (dateEnd && moment().isBefore(moment(dateEnd))) {
      this.props.changePage(10)
      return
    } else if ( moment().isBefore(moment(dateCalc))) {
     this.props.changePage(8)
    } else {
      localStorage.clear();
    }

  }
  render() {
    return (
      <div className="bg">
        {this.props.page > 0 && this.props.page <= 7 ? <Stepper /> : null}
        {this.props.page === 0 ? <Intro /> : null}
        {this.props.page > 0 && this.props.page <= 7 ? (
          <>
            <div className="container pageContent">
              {this.props.page === 1 ? <Sex /> : null}
              {this.props.page === 2 ? <PhysicalActivity /> : null}
              {this.props.page === 3 ? <Meats /> : null}
              {this.props.page === 4 ? <Allergies /> : null}
              {this.props.page === 5 ? <BadHabits /> : null}
              {this.props.page === 6 ? <Objetives /> : null}
              {this.props.page === 7 ? <Measurements /> : null}
            </div>
            <Arrow />
          </>
        ) : null}
        {this.props.page === 8 ? <Results /> : null}
        {this.props.page === 9 ? <Paypage /> : null}
        {this.props.page === 10 ? <FormPage /> : null}
      </div>
    )
  }
}
const mapStateToProps = state => ({
  page: state.page,
})
const mapDispatchToProps = dispatch => ({
  setAllState(data) {
    dispatch({
      type: 'SET_AL_STATE',
      data: data,
    })
  },
  changePage(page) {
    dispatch({
      type: 'CHANGE_PAGE',
      page: page,
    })
  },
})
export default connect(mapStateToProps, mapDispatchToProps)(Page)
