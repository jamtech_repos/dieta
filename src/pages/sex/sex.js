import React, { Component } from 'react';
import { connect } from 'react-redux';
import './sex.css';

class Sex extends Component {
  render() {
    return (
      <div className='container'>
        <div className='row'>
          <div className='section-text col-md-12 col-lg-6'>
            Cual es tu Sexo?
          </div>
          <div className='options col-md-12 col-lg-6'>
            <p>Selecciona una opción y haz click en siguiente</p>
            <div className='row'>
              <div
                onClick={() => this.props.selectSexo('hombre')}
                className={
                  'element col-6 col-sm-6 ' +
                  (this.props.sexo === 'hombre' ? 'active' : '')
                }>
                <div className='icon male'>
                  <i className='fas fa-male'></i>
                </div>
                <label className='male'>Hombre</label>
              </div>
              <div
                onClick={() => this.props.selectSexo('mujer')}
                className={
                  'element col-6 col-sm-6 ' +
                  (this.props.sexo === 'mujer' ? 'active' : '')
                }>
                <div className='icon female'>
                  <i className='fas fa-female'></i>
                </div>
                <label className='female'>Mujer</label>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}
const mapStateToProps = state => ({
  sexo: state.sexo
});
const mapDispatchToProps = dispatch => ({
  selectSexo(sexo) {
    dispatch({
      type: 'SELECT_SEX',
      sexo: sexo
    });
  }
});
export default connect(mapStateToProps, mapDispatchToProps)(Sex);
