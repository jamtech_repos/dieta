import React, { Component } from 'react'
import { connect } from 'react-redux'
class CaloriasNivel extends Component {
  convertToLb(value) {
    return (value * 2.205).toFixed(1)
  }
  render() {
    return (
      <div id="pesoideal" style={{ paddingTop: '13px', marginBottom: '-23px' }}>
        <p className="result__titles">Tu peso "ideal" es:</p>
        {this.props.type_measure === 0 ? (
          <div className="result__values" style={{ fontSize: '90px' }}>
            {this.props.peso_ideal_kg} <span>Kg</span>
          </div>
        ) : (
          <div className="result__values">
            {this.convertToLb(this.props.peso_ideal_kg)} <span>Lb</span>
          </div>
        )}
      </div>
    )
  }
}
const mapStateToProps = state => ({
  type_measure: state.type_measure,
  peso_ideal_kg: state.peso_ideal_kg,
})
const mapDispatchToProps = dispatch => ({})
export default connect(mapStateToProps, mapDispatchToProps)(CaloriasNivel)
