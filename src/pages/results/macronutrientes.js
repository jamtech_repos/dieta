import React, { Component, createRef } from 'react'
import { Slider } from 'antd'
import { connect } from 'react-redux'

import 'antd/es/slider/style/css'
class Macronutrientes extends Component {
  constructor(props) {
    super(props)
    this.state = { slider1: createRef() }
    this.changeApend = this.changeApend.bind(this)
    this.changeApend2 = this.changeApend2.bind(this)
  }
  componentDidMount() {
    const node = document.getElementsByClassName('ant-slider-handle')
    node[node.length - 2].append(this.props.proteinas_percent)
    node[node.length - 1].append(this.props.grasa_percent)
  }
  changeApend(value) {
    const node = document.getElementsByClassName('ant-slider-handle')
    node[node.length - 2].innerText = value
  }
  changeApend2(value) {
    const node = document.getElementsByClassName('ant-slider-handle')
    node[node.length - 1].innerText = value
  }
  formatter(value) {
    return `${value}%`
  }
  render() {
    return (
      <div style={{ margin: '5% auto' }}>
        <p className="result__titles" id="macro">
          Ajusta tus Macronutrientes según tu objetivo:
        </p>
        <div className="row">
          <div className="col-sm-4">
            <div className="result__values">
              {this.props.proteinas_dia_g} <span>g</span>
            </div>
            <p className="result__subtitles">DE PROTEINAS/DÍA</p>
            <Slider
              id="slider1"
              ref={this.state.slider1}
              marks={{
                1.0: {
                  style: {
                    color: '#fff',
                  },
                  label: <strong>1g</strong>,
                },
                3.0: {
                  style: {
                    color: '#fff',
                  },
                  label: <strong>3g</strong>,
                },
              }}
              value={this.props.proteinas_percent}
              min={1}
              max={3}
              step={0.1}
              onChange={value => {
                this.props.changePercentProteinas(value)
                this.changeApend(value)
              }}
            />{' '}
            <p className="result__subtitles_gauge">gr/kg/día</p>
          </div>
          <div className="col-sm-4">
            <div className="result__values">
              {this.props.grasa_dia_g} <span>g</span>
            </div>
            <p className="result__subtitles">DE GRASA/DÍA</p>
            <Slider
              marks={{
                15: {
                  style: {
                    color: '#fff',
                  },
                  label: <strong>15%</strong>,
                },
                80: {
                  style: {
                    color: '#fff',
                  },
                  label: <strong>80%</strong>,
                },
              }}
              value={this.props.grasa_percent}
              min={15}
              max={80}
              step={1}
              tipFormatter={this.formatter}
              onChange={value => {
                this.props.changePercentGrasa(value)
                this.changeApend2(value)
              }}
              id="slider"
            />
            <p className="result__subtitles_gauge">% de grasa/día</p>
          </div>
          <div className="col-sm-4">
            <div className="result__values">
              {this.props.carbohidratos_dia_g} <span>g</span>
            </div>
            <p className="result__subtitles">DE CARBOHIDRATOS/DÍA</p>
          </div>
        </div>
        <p
          style={{
            color: 'white',
            textAlign: 'center',
            margin: '15px',
          }}
        >
          *Hay 4 calorías por cada gramo de proteína y de carbohidratos. Y 9
          calorías por cada gramo de grasa.
        </p>
      </div>
    )
  }
}
const mapStateToProps = state => ({
  proteinas_dia_g: state.proteinas_dia_g,
  grasa_dia_g: state.grasa_dia_g,
  carbohidratos_dia_g: state.carbohidratos_dia_g,
  proteinas_percent: state.proteinas_percent,
  grasa_percent: state.grasa_percent,
})
const mapDispatchToProps = dispatch => ({
  changePercentProteinas(proteinas_percent) {
    dispatch({
      type: 'CHANGE_PERCENT_PROTEINAS',
      proteinas_percent,
    })
  },
  changePercentGrasa(grasa_percent) {
    dispatch({
      type: 'CHANGE_PERCEMT_GRASA',
      grasa_percent,
    })
  },
})
export default connect(mapStateToProps, mapDispatchToProps)(Macronutrientes)
