import React, { Component } from 'react'
import * as axios from 'axios'
import { connect } from 'react-redux'
import { Form, Button } from 'react-bootstrap'
import ReCAPTCHA from 'react-google-recaptcha'
import './sendyform.css'
import QueryString from 'qs'
class SendyForm extends Component {
  constructor(props) {
    super(props)
    this.handleClick = this.handleClick.bind(this)
  }
  handleClick() {
    this.props.changePage(9);
  }



  render() {
    var value = this.props.planValue.toString()
    value = value.split('.')
    return (
      <div style={{ backgroundColor: '#1a1a1a', display: 'flex' }}>
        <div
          className="container"
          style={{ margin: '40px auto', paddingBottom: '120px' }}
        >
          <div className="row" style={{display: 'flex', justifyContent: 'center'}}>
            <div className="col-lg-8" style={{ padding: '20px' }}>
              <div className="container enddescription">
                <h5>Quieres una dieta</h5>
                <h3>100% personalizada</h3>
                <div className="price">
                  <h4>
                    {value[0]} <span>{value[1]}$</span>
                  </h4>
                  <p>*Si eres socio del CTE de FullMusculo pagas solo la mitad</p>
                  {/*<p style={{ marginTop: '-40px' }}>USD $</p>*/}
                </div>
                <ul>
                  <li>Un plan de alimentación diseñado por nuestros nutricionistas.</li>
                  <li>
                    100% personalizado adaptada a tu objetivo.
                  </li>
                  <li>
                    Con una selección de alimentos tomando en cuenta
                    tus gustos, alergias, intolerancias y donde vivas.
                  </li>
                  <li>
                    Cada porción calculada en base a tus requerimientos
                    energéticos.
                  </li>
                  <li>
                    Tus comidas distribuidas a lo largo del día 
                    según tus horarios y preferencias.
                  </li>
                  <li>
                    Con un total de 6 menús y más de 18 combinaciones distintas 
                    donde eres TÚ quien elije que comer en cada una de las comidas.
                  </li>
                  <li>
                    Y una lista de compras con los alimentos que hemos incluido en tu plan de alimentación.
                  </li>
                  <li>
                    Tendrás soporte durante todo un mes a través del email, para resolver todas las dudas que se te presenten.
                  </li>
                  <li>Es un ÚNICO PAGO, sin cargos ocultos, ni suscripción.</li>
                  <li>
                    Una vez realizas el pago, tendrás que rellenar un formulario con toda tu información y recibirás tu dieta
                    en un plazo no mayor a 7 días hábiles directamente a tu email.
                  </li>
                  <li>Contamos con MÁS de 180 reviews en trustpilot con Valoración EXCELENTE por nuestros productos y servicios.</li>
                  <li><b>Además, si pides TU DIETA PERSONALIZADA HOY te llevas los siguientes BONUS:</b><br/>  Ebook con +100 recetas saludables 
                  y una Audio guía sobre como mantener el peso perdido</li>
                </ul>
                <button
                    style={{
                      margin: '30px auto',
                      backgroundColor: '#3a6ea7',
                      color: 'white',
                      textTransform: 'uppercase',
                    }}
                    onClick={this.handleClick}
                >
                  Si quiero mi dieta
                </button>
              </div>
            </div>

          </div>

        </div>
      </div>
    )
  }
}

const mapStateToProps = state => ({
  planValue: state.planValue,
})
const mapDispatchToProps = dispatch => ({
  changePage(page) {
    dispatch({
      type: 'CHANGE_PAGE',
      page,
    })
  },
})
export default connect(mapStateToProps, mapDispatchToProps)(SendyForm)
