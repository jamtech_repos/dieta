import React, { Component } from 'react'
import { connect } from 'react-redux'
import Gauge from './gauge/gauge'
import CaloriasNivel from './calorias_nivel'
import PesoIdeal from './peso_ideal'
import CaloriasObjetivo from './calorias_objetivos'
import './result.css'
import Mpm from './mpm'
import Sendyform from './sendyform/sendyform'
import FooterInfo from '../../components/footer/footerInfo'
import Macronutrientes from './macronutrientes'
import Nivelagua from './nivelagua'
import { createRef } from 'react'
import moment from 'moment'

class Results extends Component {
  constructor(props) {
    super(props)
    this.state = {
        sendyform: createRef(),
        userRestricId: localStorage.getItem('userRestricId')
    }
    this.toDown = this.toDown.bind(this)
    this.newCalc = this.newCalc.bind(this)
  }
  componentDidMount() {
    this.props.calculateResult()
    localStorage.setItem('dateCalc', moment().add(90, 'days').format())
  }
  newCalc() {
    localStorage.clear()
    window.location.reload()
  }
  toDown() {
    this.state.sendyform.current.scrollIntoView({
      behavior: 'smooth',
      block: 'start',
    })
  }
  render() {
    return (
      <>
        <div id="init-page" className="container">
          <button
            style={{
              margin: '30px auto',
              backgroundColor: 'transparent',
              color: 'white',
              textTransform: 'uppercase',
              marginTop: '16px',
              marginBottom: '30px',
              border: '2px solid #bcec4d;',
            }}
            onClick={this.toDown}
          >
            Quiero aprender a entrenar de forma efectiva
          </button>
            {!this.state.userRestricId ? <p className="no-show">
                Para poder ver e interactuar con el resultado debes estar registrado en{" "}
                <a href="https://fullmusculo.com">Fullmusculo.com</a>
            </p>:null}
          <div className="row" style={{ margin: '50px auto', filter: !this.state.userRestricId ?"brightness(0.2)":'none', pointerEvents: !this.state.userRestricId ? "none":'all'}}>

            <div className="col-lg-6">
              <Gauge />
              <PesoIdeal />
            </div>
            <div
              className="col-lg-6"
              style={{
                display: 'flex',
                flexDirection: 'column',
                justifyContent: 'space-between',
              }}
            >
              <CaloriasNivel />
              <CaloriasObjetivo />
              {this.props.state.sexo === 'hombre' ? <Mpm /> : null}
            </div>
            <div className="col-12">
              <Macronutrientes />
            </div>
            <div className="col-12">
              <Nivelagua />
            </div>
          </div>
          <button
            style={{
              margin: '0 auto',
              backgroundColor: 'transparent',
              color: 'white',
              textTransform: 'uppercase',
              marginTop: '16px',
              marginBottom: '30px',
              border: '2px solid #bcec4d;',
            }}
            onClick={this.newCalc}
          >
            Volver a calcular
          </button>
        </div>
        <div ref={this.state.sendyform} style={{ backgroundColor: '#1a1a1a' }}>
          <FooterInfo />
        </div>
      </>
    )
  }
}
const mapStateToProps = (state) => ({
  state: state,
})
const mapDispatchToProps = (dispatch) => ({
  calculateResult() {
    dispatch({
      type: 'GENERATE_RESULT',
    })
  },
  changePage(page) {
    dispatch({
      type: 'CHANGE_PAGE',
      page,
    })
  },
})
export default connect(mapStateToProps, mapDispatchToProps)(Results)
