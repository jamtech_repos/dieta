import React, { Component } from 'react'
import { connect } from 'react-redux'
import GaugeChart from 'react-gauge-chart'
import './gauge.css'
class Gauge extends Component {
  setAlturaPies(medida) {
    switch (medida) {
      case 56:
        return '4ft 8in'
      case 57:
        return '4ft 8in'
      case 58:
        return '4ft 8in'
      case 59:
        return '4ft 8in'
      case 60:
        return '5ft 0in'
      case 61:
        return '5ft 1in'
      case 62:
        return '5ft 2in'
      case 63:
        return '5ft 3in'
      case 64:
        return '5ft 4in'
      case 65:
        return '5ft 5in'
      case 66:
        return '5ft 6in'
      case 67:
        return '5ft 7in'
      case 68:
        return '5ft 8in'
      case 69:
        return '5ft 9in'
      case 70:
        return '5ft 10in'
      case 71:
        return '5ft 11in'
      case 72:
        return '6ft 0in'
      case 73:
        return '6ft 1in'
      case 74:
        return '6ft 2in'
      case 75:
        return '6ft 3in'
      case 76:
        return '6ft 4in'
      case 77:
        return '6ft 5in'
      case 78:
        return '6ft 6in'
      case 79:
        return '6ft 7in'
      case 80:
        return '6ft 8in'
      case 81:
        return '6ft 9in'
      case 82:
        return '6ft 10in'
      case 83:
        return '6ft 11in'
      case 84:
        return '4ft 0in'
      default:
        return ''
    }
  }
  convertToPercent(n1, n2) {
    let nb = (n1 * 100) / 40
    let na = (n2 * 100) / 40
    return -(nb - na)
  }
  setColorBmi(bmi) {
    if (bmi < 18.5) {
      return '#4db2ec'
    }
    if (bmi >= 18.5 && bmi <= 24.9) {
      return '#bcec4d'
    }
    if (bmi >= 25 && bmi <= 29.9) {
      return '#ecc14d'
    }
    if (bmi >= 30) {
      return '#ec4d4d'
    }
  }
  setBmiTipo(bmi) {
    if (bmi < 18.5) {
      return 'Bajo peso'
    }
    if (bmi >= 18.5 && bmi <= 24.9) {
      return 'Peso normal'
    }
    if (bmi >= 25 && bmi <= 29.9) {
      return 'Sobrepeso'
    }
    if (bmi >= 30) {
      return 'Obesidad'
    }
  }
  render() {
    return (
      <div>
        <div>
          <p className="gauge__description">
            Eres <b>{this.props.sexo}</b> de <b>{this.props.edad} años</b> con
            una altura de{' '}
            {this.props.type_measure === 0 ? (
              <b>{this.props.altura} cm</b>
            ) : (
              <b>{this.setAlturaPies(this.props.altura)}</b>
            )}
          </p>
          <p className="gauge__description">
            {' '}
            y pesas{' '}
            <b>
              {this.props.peso} {this.props.type_measure === 0 ? 'kg' : 'lb'}
            </b>{' '}
            con <b>{this.setBmiTipo(this.props.bmi)}</b>
          </p>
        </div>
        <GaugeChart
          id="gauge-chart2"
          nrOfLevels={4}
          arcsLength={[
            this.convertToPercent(0, 18.5),
            this.convertToPercent(18.5, 25),
            this.convertToPercent(25, 30),
            this.convertToPercent(30, 40),
          ]}
          colors={['#4db2ec', '#bcec4d', '#ecc14d', '#ec4d4d']}
          percent={this.props.bmi <= 40 ? (this.props.bmi * 100) / 40 / 100 : 1}
          needleColor={this.setColorBmi(this.props.bmi)}
          needleBaseColor={this.setColorBmi(this.props.bmi)}
          arcWidth={0.2}
          hideText={true}
          arcPadding={0.01}
        />
        <div className="gauge__content">
          <div className="gauge__peso">
            <p style={{ color: 'white' }}>Estás en </p>
            <p style={{ color: this.setColorBmi(this.props.bmi) }}>
              {this.setBmiTipo(this.props.bmi)}
            </p>
          </div>
          <div className="gauge__value">
            <p>{this.props.bmi}</p>
            <p className="bmi">
              BMI/IMC (Esto NO es tu porcentaje de grasa corporal)
            </p>
          </div>
        </div>
      </div>
    )
  }
}
const mapStateToProps = state => ({
  type_measure: state.type_measure,
  sexo: state.sexo,
  edad: state.edad,
  peso: state.peso,
  altura: state.altura,
  bmi: state.bmi,
})
const mapDispatchToProps = dispatch => ({})
export default connect(mapStateToProps, mapDispatchToProps)(Gauge)
