const calculateResult = (state) => {
    //CALCULAR BMR
    let peso_kg = state.type_measure === 0 ? state.peso : state.peso / 2.2046;
    let altura_cm = state.type_measure === 0 ? state.altura : state.altura * 2.54;
    let valor_sexo_1 = state.sexo === 'hombre' ? 5 : -161;
    let bmr = Math.round(
        9.99 * peso_kg + 6.25 * altura_cm - 4.92 * state.edad + valor_sexo_1
    );
    //CALCULAR CALORIAS SEGUN EL NIVEL DE ACTIVIDAD FISICA
    let calorias_nivel = Math.round(
        bmr * getActividadFisica(state.actividad_fisica)
    );
    //CALCULAR BMI/IMC
    let altura_mts = state.type_measure === 0 ? state.altura / 100 : state.altura / 39.37;
    let bmi = (peso_kg / (altura_mts * altura_mts)).toFixed(1);

    //CALCULAR PESO IDEAL
    let altura_in = state.type_measure === 0 ? state.altura / 2.54 : state.altura;
    let valor_sexo_2 = state.sexo === 'hombre' ? 52 : 49;
    let valor_sexo_3 = state.sexo === 'hombre' ? 1.9 : 1.7;
    let altura_60 = altura_in > 60 ? altura_in - 60 : 60 - altura_in;
    let peso_ideal_kg = Math.round(valor_sexo_2 + valor_sexo_3 * altura_60);

    //CALCULAR MPM
    let mpm_superior = (altura_cm - 98);
    let mpm_inferior = (altura_cm - 102);

    //CALCULAR CALORIAS SEGUN OBJETIVO
    let calorias_editor = state.objetivo !== 2 ? 500 : 0
    let calorias_objetivo = calorias_nivel
    let calorias = state.objetivo === 1 ?
        calorias_objetivo + calorias_editor :
        calorias_objetivo - calorias_editor

    //CALCULAR MACRONUTRIENTES
    let grasa_dia_cal = Math.round(calorias * (state.grasa_percent / 100));
    let proteinas_dia_cal = Math.round((peso_kg * state.proteinas_percent) * 4)

    let proteinas_dia_g = Math.round(proteinas_dia_cal / 4);
    let grasa_dia_g = Math.round(grasa_dia_cal / 9);
    let carbohidratos_dia_g = Math.round((calorias - proteinas_dia_cal - grasa_dia_cal) / 4);

    return {
        bmr,
        calorias_nivel,
        bmi,
        peso_ideal_kg,
        mpm_superior,
        mpm_inferior,
        calorias_objetivo,
        proteinas_dia_g,
        grasa_dia_g,
        carbohidratos_dia_g,
        calorias_editor
    }
}

function getActividadFisica(value) {
    switch (value) {
        case 'S':
            return 1.2;
        case 'EL':
            return 1.37;
        case 'EM':
            return 1.55;
        case 'ED':
            return 1.73;
        case 'A':
            return 1.9;
        default:
            return 0;
    }
}


function getCaloriasObjetivo(calorias_nivel, objetivo) {
    switch (objetivo) {
        case 1:
            return calorias_nivel + 500;
        case 2:
            return calorias_nivel;
        case 3:
            return calorias_nivel - 500;
        default:
            return 0
    }
}
export {
    calculateResult,
    getCaloriasObjetivo,
    getActividadFisica
}