import React, { Component } from 'react'
import { connect } from 'react-redux'

class Mpm extends Component {
  convertToLb(value) {
    return Math.round(value * 2.205)
  }
    isFloat(x) { return !!(x % 1); }

    render() {
    return (
      <div>
        <p className="result__titles" id="maxpotencial">
          TU MÁXIMO POTENCIAL MUSCULAR ES DE:
        </p>
        {this.props.type_measure === 0 ? (
          <div id="mpmvalue" className="result__values">
            {this.isFloat(this.props.mpm_inferior) ? this.props.mpm_inferior.toFixed(1): this.props.mpm_inferior } <span>Kg</span> a{' '}
            {this.isFloat(this.props.mpm_superior) ? this.props.mpm_superior.toFixed(1): this.props.mpm_superior } <span>Kg</span>
          </div>
        ) : (
          <div className="result__values">
            {this.isFloat(this.convertToLb(this.props.mpm_inferior)) ? this.convertToLb(this.props.mpm_inferior).toFixed(1):this.convertToLb(this.props.mpm_inferior)} <span>Lb</span> a{' '}
            {this.isFloat(this.convertToLb(this.props.mpm_superior)) ? this.convertToLb(this.props.mpm_superior).toFixed(1):this.convertToLb(this.props.mpm_superior)} <span>Lb</span>
          </div>
        )}
        <p className="result__subtitles">con un 5-6% de grasa corporal</p>
      </div>
    )
  }
}
const mapStateToProps = state => ({
  mpm_inferior: state.mpm_inferior,
  mpm_superior: state.mpm_superior,
  type_measure: state.type_measure,
})
const mapDispatchToProps = dispatch => ({})
export default connect(mapStateToProps, mapDispatchToProps)(Mpm)
