import React, { Component } from 'react';
import { Slider } from 'antd';
import { connect } from 'react-redux';

import 'antd/es/slider/style/css';
class CaloriasObjetivo extends Component {
  setObjetive(obj) {
    switch (obj) {
      case 1:
        return 'Aumentar masa muscular';
      case 2:
        return 'Mantenimiento';
      case 3:
        return 'Perder grasa corporal';
      default:
        return '';
    }
  }
  render() {
    return (
      <div>
        <p className='result__titles' id="calrecomendadas">Tus calorias recomendadas son:</p>
        <div className='result__values'>
          {this.props.objetivo === 1
            ? this.props.calorias_objetivo + this.props.calorias_editor
            : this.props.calorias_objetivo - this.props.calorias_editor}{' '}
          <span>cal</span>
        </div>
        <p className='result__subtitles'>Necesarias para lograr tu objetivo de:</p>
        <p className='result__titles' style={{ margin: '0' }}>
          {this.setObjetive(this.props.objetivo)}
        </p>
        {this.props.objetivo !== 2 ? (
          <Slider
            marks={{
              0: {
                style: {
                  color: '#fff'
                },
                label: <strong>0 cal</strong>
              },
              500: {
                style: {
                  color: '#fff'
                },
                label: <strong>500 cal</strong>
              }
            }}
            value={this.props.calorias_editor}
            min={0}
            max={500}
            onChange={this.props.changeSlider}
            id='slider'
          />
        ) : null}
        {this.props.objetivo === 1 ? (
          <p className='result__subtitles_gauge'>Superávit Calórico</p>
        ) : null}
        {this.props.objetivo === 3 ? (
          <p className='result__subtitles_gauge'>Déficit Calórico</p>
        ) : null}
      </div>
    );
  }
}
const mapStateToProps = state => ({
  calorias_objetivo: state.calorias_objetivo,
  calorias_nivel: state.calorias_nivel,
  calorias_editor: state.calorias_editor,
  objetivo: state.objetivo
});
const mapDispatchToProps = dispatch => ({
  changeSlider(input) {
    dispatch({
      type: 'CHANGE_CALORIAS_EDITOR',
      calorias_editor: input
    });
  }
});
export default connect(mapStateToProps, mapDispatchToProps)(CaloriasObjetivo);
