import React, { Component } from 'react'
import { connect } from 'react-redux'

class NivelAgua extends Component {
  render() {
    return (
      <div>
        <p className="result__titles" id="recomendados">
          Tu recomendación de consumo de agua al día es de:
        </p>
        <div
          className="row"
          style={{ alignItems: 'center', justifyContent: 'center' }}
        >
          <div className="result__values">
            {(
              (this.props.type_measure === 0
                ? this.props.peso
                : this.props.peso / 2.2046) / 30
            ).toFixed(1)}{' '}
            <span>Lts</span>
          </div>
          <div
            style={{
              display: 'flex',
              flexDirection: 'column',
              alignItems: 'center',
              marginBottom: '5px',
            }}
            id="orina"
          >
            <p
              style={{
                color: 'white',
                display: 'flex',
                alignItems: 'center',
                margin: '5px',
              }}
            >
              Color de la Orina
            </p>
            <div className="row watericons">
              <div className="row ">
                <div
                  className="nivel"
                  style={{
                    backgroundColor: '#ffffff',
                  }}
                ></div>
                <div
                  className="nivel"
                  style={{
                    backgroundColor: '#f8f4e9',
                  }}
                ></div>
                <div
                  className="nivel"
                  style={{
                    backgroundColor: '#f8ecb8',
                  }}
                ></div>
                <p>Hidratado</p>
              </div>
              <div className="row">
                <div
                  className="nivel"
                  style={{
                    backgroundColor: '#f9e491',
                  }}
                ></div>
                <div
                  className="nivel"
                  style={{
                    backgroundColor: '#f8de63',
                  }}
                ></div>
                <div
                  className="nivel"
                  style={{
                    backgroundColor: '#fcd925',
                  }}
                ></div>
                <p>Deshidratado</p>
              </div>
              <div className="row">
                <div
                  className="nivel"
                  style={{
                    backgroundColor: '#efca00',
                  }}
                ></div>
                <div
                  className="nivel"
                  style={{
                    backgroundColor: '#d4b000',
                  }}
                ></div>
                <div
                  className="nivel"
                  style={{
                    backgroundColor: '#756100',
                  }}
                ></div>
                <p id="desdgrave">Deshidratación Grave</p>
              </div>
            </div>
          </div>
        </div>
      </div>
    )
  }
}
const mapStateToProps = state => ({
  peso: state.peso,
  type_measure: state.type_measure,
})
const mapDispatchToProps = ddivspatch => ({})
export default connect(mapStateToProps, mapDispatchToProps)(NivelAgua)
