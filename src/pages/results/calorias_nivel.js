import React, { Component } from 'react'
import { connect } from 'react-redux'
class CaloriasNivel extends Component {
  constructor(props) {
    super(props)
    this.state = {
      width: 768,
    }
  }
  componentDidMount() {
    window.addEventListener('resize', this.updateWindowDimensions())
  }

  componentWillUnmount() {
    window.removeEventListener('resize', this.updateWindowDimensions)
  }
  updateWindowDimensions() {
    this.setState({ width: window.innerWidth })
  }
  render() {
    return (
      <div>
        <p className="result__titles" id="calmantenimiento">
          Tus calorias de mantenimiento son:
        </p>
        <div className="result__values">
          {this.props.calorias_nivel} <span>cal</span>
        </div>
        <p className="result__subtitles">De acuerdo a tu nivel de actividad:</p>
        {this.state.width < 767 ? (
          <>
            <div
              className="result__pactivity options"
              style={{ padding: '0px' }}
            >
              <button
                className={
                  this.props.actividad_fisica === 'S' ? 'active' : null
                }
                onClick={() => {
                  this.props.selectOption('S')
                }}
              >
                0
              </button>
              <button
                className={
                  this.props.actividad_fisica === 'EL' ? 'active' : null
                }
                onClick={() => {
                  this.props.selectOption('EL')
                }}
              >
                1-2
              </button>
              <button
                className={
                  this.props.actividad_fisica === 'EM' ? 'active' : null
                }
                onClick={() => {
                  this.props.selectOption('EM')
                }}
              >
                3-5
              </button>
              <button
                className={
                  this.props.actividad_fisica === 'ED' ? 'active' : null
                }
                onClick={() => {
                  this.props.selectOption('ED')
                }}
              >
                6-7
              </button>
              <button
                className={
                  this.props.actividad_fisica === 'A' ? 'active' : null
                }
                onClick={() => {
                  this.props.selectOption('A')
                }}
              >
                2x
              </button>
            </div>
            <p style={{ textAlign: 'center', color: 'white' }}>Días</p>
          </>
        ) : (
          <div className="result__pactivity options" style={{ padding: '0px' }}>
            <button
              className={this.props.actividad_fisica === 'S' ? 'active' : null}
              onClick={() => {
                this.props.selectOption('S')
              }}
            >
              Sedentario
            </button>
            <button
              className={this.props.actividad_fisica === 'EL' ? 'active' : null}
              onClick={() => {
                this.props.selectOption('EL')
              }}
            >
              1-2 días
            </button>
            <button
              className={this.props.actividad_fisica === 'EM' ? 'active' : null}
              onClick={() => {
                this.props.selectOption('EM')
              }}
            >
              3-5 días
            </button>
            <button
              className={this.props.actividad_fisica === 'ED' ? 'active' : null}
              onClick={() => {
                this.props.selectOption('ED')
              }}
            >
              6-7 días
            </button>
            <button
              className={this.props.actividad_fisica === 'A' ? 'active' : null}
              onClick={() => {
                this.props.selectOption('A')
              }}
            >
              2x día
            </button>
          </div>
        )}
      </div>
    )
  }
}
const mapStateToProps = state => ({
  calorias_nivel: state.calorias_nivel,
  actividad_fisica: state.actividad_fisica,
})
const mapDispatchToProps = dispatch => ({
  selectOption(actividad_fisica) {
    dispatch({
      type: 'SELECT_ACTIVIDAD_FISICA',
      actividad_fisica: actividad_fisica,
    })
  },
})
export default connect(mapStateToProps, mapDispatchToProps)(CaloriasNivel)
