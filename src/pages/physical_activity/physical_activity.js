import React, { Component } from 'react'
import { connect } from 'react-redux'
import './physical_activity.css'
class PhysicalActivity extends Component {
  render() {
    return (
      <div className="container">
        <div className="row">
          <div id="actividadfisica" className="section-text col-md-12 col-lg-6">
            Actividad física
          </div>
          <div className="options col-md-12 col-lg-6">
            <p className="textHelp">
              Selecciona una opción y haz click en siguiente
            </p>
            <button
              className={this.props.actividad_fisica === 'S' ? 'active' : null}
              onClick={() => {
                this.props.selectOption('S')
              }}
            >
              Sedentario (Trabajo de oficina)
            </button>
            <button
              className={this.props.actividad_fisica === 'EL' ? 'active' : null}
              onClick={() => {
                this.props.selectOption('EL')
              }}
            >
              ejercicio leve (1 a 2 días por semana)
            </button>
            <button
              className={this.props.actividad_fisica === 'EM' ? 'active' : null}
              onClick={() => {
                this.props.selectOption('EM')
              }}
            >
              ejercicio moderado (3 a 5 días por semana)
            </button>
            <button
              className={this.props.actividad_fisica === 'ED' ? 'active' : null}
              onClick={() => {
                this.props.selectOption('ED')
              }}
            >
              ejercicio duro (6 a 7 días por semana)
            </button>
            <button
              className={this.props.actividad_fisica === 'A' ? 'active' : null}
              onClick={() => {
                this.props.selectOption('A')
              }}
            >
              Atleta (2 veces al día)
            </button>
          </div>
        </div>
      </div>
    )
  }
}
const mapStateToProps = state => ({
  actividad_fisica: state.actividad_fisica,
})
const mapDispatchToProps = dispatch => ({
  selectOption(actividad_fisica) {
    dispatch({
      type: 'SELECT_ACTIVIDAD_FISICA',
      actividad_fisica: actividad_fisica,
    })
  },
})
export default connect(mapStateToProps, mapDispatchToProps)(PhysicalActivity)
