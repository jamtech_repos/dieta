import React, { Component } from 'react'
import { connect } from 'react-redux'
import logo from '../../assets/img/logoHeader.svg'
import './endpage.css'

class EndPage extends Component {
  componentDidMount() {
    localStorage.clear()
  }
  reload() {
    localStorage.clear()
    window.location.reload()
  }
  render() {
    return (
      <div className="container endpage" style={{ padding: '0 11%' }}>
        <div style={{ padding: '100px 0' }}>
          <div style={{ display: 'flex' }}>
            <img
              style={{ margin: '0 auto' }}
              src={logo}
              alt="fullmusculo-logo"
            />
          </div>
          <div class="title">
            <h1>¡FELICIDADES!</h1>
          </div>
          <div class="subtitle">
            <p style={{ color: 'white' }}>
              Ya estamos trabajando en tu dieta personalizada y te la enviaremos
              directo a tu email en un plazo de 5-7 dias HÁBILES/LABORABLES
            </p>
            <p style={{ color: 'white' }}>
              Si compartes nuestra calculadora con 3 amig@s estarás participando por un
              año de asesoría totalmente gratis en dieta y entrenamiento.
            </p>
            <p style={{ color: 'white' }}><a href="https://api.whatsapp.com/send?text=Usa%20GRATIS%20la%20nueva%20calculadora%20de%20calorias%20y%20macronutrientes%20de%20fullmusculo%20https://fullmusculo.com/calculadora-de-calorias/?utm_source=whatsapp&utm_medium=social&utm_campaign=calculadora">CLICK AQUÍ PARA COMPARTIR EN WHATSAPP</a></p>
          </div>
          <div class="row" style={{ marginTop: '36px', marginBottom: '50px' }}>
            <div className="col-md-6">
              <p className="recalculate" onClick={this.reload}>
                Volver a la calculadora
              </p>
            </div>
            <div style={{ color: 'white' }} className="resend col-md-6">
              <p style={{ fontSize: '12px' }}>
                No olvides revisar siempre la bandeja de Spam o correo no deseado, pudiera
                darse el caso de que nuestros emails te estén llegando allí.
              </p>
              <p style={{ fontSize: '12px' }}>
                En caso de tener alguna duda o consulta puedes escribirnos
                directamente a nuestro email <b>info@fullmusculo.com</b>.
              </p>
            </div>
          </div>
        </div>
      </div>
    )
  }
}

const mapStateToProps = state => ({})
const mapDispatchToProps = dispatch => ({})
export default connect(mapStateToProps, mapDispatchToProps)(EndPage)
