import React, { Component } from 'react'
import { connect } from 'react-redux'
import './measurements.css'
import QueryString from 'qs'
import { Form } from 'react-bootstrap'
import ReCAPTCHA from 'react-google-recaptcha'
import * as axios from 'axios'
import {
  GoogleReCaptcha,
  GoogleReCaptchaProvider,
} from 'react-google-recaptcha-v3'
class Measurements extends Component {
  constructor(props) {
    super(props)
    this.state = {
      recaptchaRef: React.createRef(),
      name: '',
      email: '',
      captcha: null,
      gdpr: false,
      list: '0NM7PdmDc9tzduhnusyuNQ',
      emailValid: false,
      nameValid: false,
      formValid: false,
      showTerms: false,
      click: false,
    }
    this.SendToSendy = this.SendToSendy.bind(this)
    this.activeButom = this.activeButom.bind(this)
    this.handleClick = this.handleClick.bind(this)
    this.validateAltura = this.validateAltura.bind(this)
    this.validateEdad = this.validateEdad.bind(this)
    this.validatePeso = this.validatePeso.bind(this)
    this.handleUserInput = this.handleUserInput.bind(this)
    this.validateField = this.validateField.bind(this)
    this.validateForm = this.validateForm.bind(this)
    this.handleVerify = this.handleVerify.bind(this)
    this.focus = this.focus.bind(this)
  }
  setActive(i) {
    switch (i) {
      case 0:
        return 'Métrico'
      case 1:
        return 'Imperial'
      default:
        return 'Métrico'
    }
  }
  validateEdad() {
    if (this.props.edad) {
      if (this.props.edad >= 18 && this.props.edad <= 99) return 'valid'
      else return 'danger'
    } else return ''
  }
  validatePeso() {
    if (this.props.peso) {
      if (this.props.type_measure === 0) {
        if (this.props.peso >= 30 && this.props.peso < 200) return 'valid'
        else return 'danger'
      } else {
        if (this.props.peso >= 66 && this.props.peso < 440) return 'valid'
        else return 'danger'
      }
    }
  }
  validateAltura() {
    if (this.props.altura) {
      if (this.props.type_measure === 1) {
        if (this.props.altura >= 55 && this.props.altura <= 84) return 'valid'
        else return 'danger'
      } else {
        if (this.props.altura >= 139 && this.props.altura < 220) return 'valid'
        else return 'danger'
      }
    } else return ''
  }
  handleUserInput(e) {
    var name = e.target.name
    var value = name === 'gdpr' ? !this.state.gdpr : e.target.value
    this.setState({ [name]: value }, () => this.validateField(name, value))
  }
  activeButom() {
    return (
      this.state.formValid &&
      this.validateAltura() === 'valid' &&
      this.validateEdad() === 'valid' &&
      this.validatePeso() === 'valid'
    )
  }
  SendToSendy() {
    if (this.state.formValid) {
      var body = QueryString.stringify({
        name: this.state.name,
        email: this.state.email,
        gdpr: this.state.gdpr,
        list: this.state.list,
        subform: 'yes',
        boolean: true,
      })
      axios
        .post('https://fullmusculo.com/sendy2/subscribe', body, {
          headers: { 'Content-Type': 'application/x-www-form-urlencoded' },
        })
        .then((response) => {
          console.log(response)
          if (response.data) {
            this.props.changePage(12)
          }
        })
    }
  }
  async handleClick(event) {
    event.preventDefault()
    this.setState({ click: true })

    this.props.changePage(12)
    if (this.activeButom() && this.state.captcha) {
      this.SendToSendy()
    } else {
      return false
    }
  }
  validateField(fieldName, value) {
    let emailValid = this.state.emailValid
    let nameValid = this.state.nameValid

    switch (fieldName) {
      case 'email':
        emailValid = value.match(/^([\w.%+-]+)@([\w-]+\.)+([\w]{2,})$/i)
        break
      case 'name':
        nameValid = value.length >= 3
        break
      default:
        break
    }
    this.setState(
      {
        emailValid: emailValid,
        nameValid: nameValid,
      },
      this.validateForm()
    )
  }

  validateForm() {
    this.setState({
      formValid:
        this.state.emailValid && this.state.nameValid && this.state.gdpr,
    })
  }
  handleVerify(token) {
    this.setState({ captcha: token })
  }
  focus(event) {
    console.log(event.target.offsetTop)
    window.scroll({
      top: event.target.offsetBottom,
      behavior: 'smooth',
    })
  }
  render() {
    return (
      <div className="container" style={{ height: '800px' }}>
        <GoogleReCaptchaProvider
          reCaptchaKey="6Lc9X44UAAAAAOJ3ZnJSUQpiV7gfoTD-bsl4dyLl"
          useRecaptchaNet="true"
          language="es"
          scriptProps={{
            async: true, // optional, default to false,
            defer: false, // optional, default to false
            appendTo: 'head', // optional, default to "head", can be "head" or "body",
            nonce: undefined, // optional, default undefined
          }}
        >
          <div className="row">
            <div className="section-text col-md-12 col-lg-6">Medidas</div>
            <div className="options text-start col-md-12 col-lg-6">
              <div className="type">
                <p className="active">
                  {this.setActive(this.props.type_measure)}
                </p>
                <p
                  onClick={() =>
                    this.props.changeTypeMeasures(
                      this.props.type_measure === 0 ? 1 : 0
                    )
                  }
                >
                  {this.setActive(this.props.type_measure === 0 ? 1 : 0)}
                </p>
              </div>

              <form onSubmit={() => this.handleClick()}>
                <div className="row">
                  <div className="col-12 col-md-4">
                    <input
                      type="number"
                      placeholder="Edad"
                      className={this.validateEdad()}
                      maxLength={2}
                      onChange={(event) => this.props.changeEdad(event)}
                      defaultValue={this.props.edad}
                      step={6}
                      pattern="[0-9]"
                    />
                  </div>

                  <div className="group-input col-12 col-md-4">
                    <input
                      type="number"
                      className={this.validatePeso()}
                      onChange={(event) => this.props.changePeso(event)}
                      value={this.props.peso}
                      placeholder="Peso"
                      maxLength={6}
                    />
                    {this.props.type_measure === 0 ? (
                      <span>kg</span>
                    ) : (
                      <span>lbs</span>
                    )}
                  </div>
                  {this.props.type_measure === 0 ? (
                    <div className="group-input col-12 col-md-4">
                      <input
                        type="number"
                        className={this.validateAltura()}
                        onChange={(event) => this.props.changeAltura(event)}
                        defaultValue={this.props.altura}
                        placeholder="Altura"
                        maxLength="6"
                      />
                      <span>cm</span>
                    </div>
                  ) : (
                    <div className="group-input col-12 col-md-4">
                      <select
                        onChange={(event) => this.props.changeAltura(event)}
                        className={this.validateAltura()}
                        value={this.props.altura}
                        placeholder="Altura"
                      >
                        <option disabled>Altura</option>
                        <option value={55}> 4ft 7in</option>
                        <option value={56}> 4ft 8in</option>
                        <option value={57}> 4ft 9in</option>
                        <option value={58}> 4ft 10in</option>
                        <option value={59}> 4ft 11in</option>
                        <option value={60}> 5ft 0in</option>
                        <option value={61}> 5ft 1in</option>
                        <option value={62}> 5ft 2in</option>
                        <option value={63}> 5ft 3in</option>
                        <option value={64}> 5ft 4in</option>
                        <option value={65}> 5ft 5in</option>
                        <option value={66}> 5ft 6in</option>
                        <option value={67}> 5ft 7in</option>
                        <option value={68}> 5ft 8in</option>
                        <option value={69}> 5ft 9in</option>
                        <option value={70}> 5ft 10in</option>
                        <option value={71}> 5ft 11in</option>
                        <option value={72}> 6ft 0in</option>
                        <option value={73}> 6ft 1in</option>
                        <option value={74}> 6ft 2in</option>
                        <option value={75}> 6ft 3in</option>
                        <option value={76}> 6ft 4in</option>
                        <option value={77}> 6ft 5in</option>
                        <option value={78}> 6ft 6in</option>
                        <option value={79}> 6ft 7in</option>
                        <option value={80}> 6ft 8in</option>
                        <option value={81}> 6ft 9in</option>
                        <option value={82}> 6ft 10in</option>
                        <option value={83}> 6ft 11in</option>
                        <option value={84}> 7ft 0in</option>
                      </select>
                    </div>
                  )}
                </div>

                <div className={'form-end'}>
                  <input
                    name="name"
                    value={this.state.name}
                    type="text"
                    placeholder="Aquí tu primer nombre..."
                    className={
                      this.state.name !== ''
                        ? this.state.nameValid
                          ? 'valid'
                          : 'danger'
                        : ''
                    }
                    onChange={(event) => this.handleUserInput(event)}
                    required={true}
                  />
                  <input
                    name="email"
                    value={this.state.email}
                    type="email"
                    placeholder="Y aquí tu email..."
                    className={
                      this.state.email !== ''
                        ? this.state.emailValid
                          ? 'valid'
                          : 'danger'
                        : ''
                    }
                    onChange={(event) => this.handleUserInput(event)}
                    required
                  />
                  {/*<input
                          name="gdpr"
                          checked={this.state.gdpr}
                          onChange={event => this.handleUserInput(event)}
                          type="checkbox"
                          id="gdpr"
                          className="form-control"
                      />
                      <label htmlFor="gdpr"> Estoy de acuerdo</label>*/}
                  <div
                    style={{
                      display: 'flex',
                      justifyContent: 'space-between',
                      alignItems: 'center',
                    }}
                  >
                    <div className="custom-control custom-checkbox">
                      <input
                        className="custom-control-input danger"
                        type="checkbox"
                        id="gdpr"
                        name="gdpr"
                        checked={this.state.gdpr}
                        onChange={(event) => this.handleUserInput(event)}
                      />
                      <label
                        className={
                          'custom-control-label ' +
                          (this.state.click && !this.state.gdpr
                            ? 'danger'
                            : null)
                        }
                        style={{ padding: '0' }}
                        htmlFor="gdpr"
                      >
                        ESTOY DE ACUERDO
                      </label>
                    </div>
                    <label
                      className="mobile"
                      style={{ fontSize: '10px', margin: '0' }}
                    >
                      <b
                        onClick={() =>
                          this.setState({ showTerms: !this.state.showTerms })
                        }
                      >
                        Ver los terminos
                      </b>
                    </label>
                  </div>

                  <GoogleReCaptcha onVerify={this.handleVerify} />
                  {this.state.showTerms ? (
                    <div className="acuerdo mobile">
                      <p style={{ textAlign: 'start' }}>
                        <b>Permiso de mercadeo:</b> Doy mi consentimiento para
                        que FullMusculo esté en contacto conmigo por email
                        utilizando la información que he proporcionado en este
                        formulario con el fin de recibir noticias,
                        actualizaciones y marketing.
                      </p>
                      <p style={{ textAlign: 'start' }}>
                        <b>Que esperar:</b> Si desea retirar su consentimiento y
                        dejar de recibir noticias nuestras, simplemente haga
                        click en el enlace para darse de baja en la parte
                        inferior de cada email que le enviamos o contáctenos a
                        info@fullmusculo.com. Valoramos y respetamos sus datos
                        personales y privacidad. Para ver nuestra política de
                        privacidad, visite nuestro sitio web. Al enviar este
                        formulario, acepta que podamos procesar su información
                        de acuerdo con estos términos.
                      </p>
                    </div>
                  ) : null}
                  <div className="acuerdo nomobile">
                    <p style={{ textAlign: 'start' }}>
                      <b>Permiso de mercadeo:</b> Doy mi consentimiento para que
                      FullMusculo esté en contacto conmigo por email utilizando
                      la información que he proporcionado en este formulario con
                      el fin de recibir noticias, actualizaciones y marketing.
                    </p>
                    <p style={{ textAlign: 'start' }}>
                      <b>Que esperar:</b> Si desea retirar su consentimiento y
                      dejar de recibir noticias nuestras, simplemente haga click
                      en el enlace para darse de baja en la parte inferior de
                      cada email que le enviamos o contáctenos a
                      info@fullmusculo.com. Valoramos y respetamos sus datos
                      personales y privacidad. Para ver nuestra política de
                      privacidad, visite nuestro sitio web. Al enviar este
                      formulario, acepta que podamos procesar su información de
                      acuerdo con estos términos.
                    </p>
                  </div>
                </div>
                <div
                  style={{
                    display: 'flex',
                    justifyContent: 'space-between',
                    alignItems: 'center',
                  }}
                >
                  <label
                    style={{ fontWeight: 'bold', margin: 0 }}
                    onClick={() => this.props.changePage(this.props.page - 1)}
                  >
                    <i className="fas fa-angle-left" /> ANTERIOR
                  </label>
                  <button
                    type="submit"
                    /* className={this.activeButom() ? 'done' : ''} */
                    onClick={this.handleClick}
                    style={{
                      margin: 0,
                      width: '65%',
                      backgroundColor: '#3a6ea7',
                      marginLeft: '20px',
                      color: 'white',
                    }}
                  >
                    Calcular
                  </button>
                </div>
              </form>
            </div>
          </div>
        </GoogleReCaptchaProvider>
      </div>
    )
  }
}
const mapStateToProps = (state) => ({
  type_measure: state.type_measure,
  edad: state.edad,
  peso: state.peso,
  altura: state.altura,
  page: state.page,
  state: state,
})
const mapDispatchToProps = (dispatch) => ({
  changeTypeMeasures(type) {
    dispatch({
      type: 'CHANGE_TYPE_MEASURE',
      type_measure: type,
    })
  },
  changeEdad(event) {
    dispatch({
      type: 'CHANGE_EDAD',
      edad: event.target.value.slice(0, 2),
    })
  },
  changePeso(event) {
    dispatch({
      type: 'CHANGE_PESO',
      peso: event.target.value,
    })
  },
  changeAltura(event) {
    dispatch({
      type: 'CHANGE_ALTURA',
      altura: Number.parseInt(event.target.value),
    })
  },
  changePage(page) {
    dispatch({
      type: 'CHANGE_PAGE',
      page: page,
    })
  },
})
export default connect(mapStateToProps, mapDispatchToProps)(Measurements)
