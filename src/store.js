import { createStore } from 'redux'
/* import { createRef } from 'react';*/
import update from 'react-addons-update'
import { calculateResult, getActividadFisica } from './pages/results/calculate'

const initialState = {
  page: 0,
  type_measure: 0,

  sexo: '',
  actividad_fisica: '',
  carnes: [false, false, false, false, false, false],
  alergias: [false, false, false, false, false, false],
  maloshabitos: [false, false, false, false, false, false],
  objetivo: 0,
  edad: '',
  altura: '',
  peso: '',

  bmr: 0,
  calorias_nivel: 0,
  bmi: 0,
  peso_ideal_kg: 0,
  mpm_superior: 0,
  mpm_inferior: 0,
  calorias_objetivo: 0,
  proteinas_dia_g: 0,
  grasa_dia_g: 0,
  carbohidratos_dia_g: 0,

  planValue: 194,
  totalValue: 194,
  dietaValue: 97,
  donationValue: 3,

  proteinas_percent: 1.8,
  grasa_percent: 25,
  calorias_editor: 500,
}

const reducer = (state = initialState, action) => {
  switch (action.type) {
    case 'CHANGE_PAGE': {
      return {
        ...state,
        page: action.page,
      }
    }
    case 'CHANGE_TYPE_MEASURE': {
      if (action.type_measure === 1) {
        return {
          ...state,
          type_measure: action.type_measure,
          peso: Number.parseFloat((state.peso * 2.2046).toFixed(2)),
          altura: Math.round(state.altura / 2.54),
        }
      } else {
        return {
          ...state,
          type_measure: action.type_measure,
          peso: Number.parseFloat((state.peso / 2.2046).toFixed(2)),
          altura: Number.parseFloat((state.altura * 2.54).toFixed(2)),
        }
      }
    }
    case 'SELECT_SEX': {
      return {
        ...state,
        sexo: action.sexo,
      }
    }
    case 'SELECT_ACTIVIDAD_FISICA': {
      if (state.page === 8) {
        let calorias_nivel = Math.round(
          state.bmr * getActividadFisica(action.actividad_fisica)
        )
        let calorias_objetivo = calorias_nivel
        let calorias_editor = state.objetivo !== 2 ? 500 : 0
        let calorias =
          state.objetivo === 1
            ? calorias_objetivo + calorias_editor
            : calorias_objetivo - calorias_editor

        let peso_kg =
          state.type_measure === 0 ? state.peso : state.peso / 2.2046

        let grasa_dia_cal = Math.round(calorias * (25 / 100))
        let proteinas_dia_cal = Math.round(peso_kg * 1.8 * 4)
        let carbohidratos_dia_g = Math.round(
          (calorias - grasa_dia_cal - proteinas_dia_cal) / 4
        )
        console.log(grasa_dia_cal + proteinas_dia_cal + carbohidratos_dia_g * 4)
        let grasa_dia_g = Math.round(grasa_dia_cal / 9)
        return {
          ...state,
          actividad_fisica: action.actividad_fisica,
          calorias_nivel,
          calorias_objetivo,
          proteinas_percent: 1.8,
          proteinas_dia_g: Math.round(proteinas_dia_cal / 4),
          grasa_percent: 25,
          calorias_editor: calorias_editor,
          carbohidratos_dia_g: carbohidratos_dia_g,
          grasa_dia_g: grasa_dia_g,
        }
      } else
        return {
          ...state,
          actividad_fisica: action.actividad_fisica,
        }
    }
    case 'SELECT_MEAT': {
      if (action.optionmeat !== 5) {
        return update(state, {
          carnes: {
            [action.optionmeat]: {
              $set: !state.carnes[action.optionmeat],
            },
            [state.carnes.length - 1]: {
              $set: false,
            },
          },
        })
      } else {
        return update(state, {
          carnes: {
            $set: [false, false, false, false, false, true],
          },
        })
      }
    }
    case 'SELECT_ALLERGIES': {
      if (action.optionallergies !== 5) {
        return update(state, {
          alergias: {
            [action.optionallergies]: {
              $set: !state.alergias[action.optionallergies],
            },
            [state.alergias.length - 1]: {
              $set: false,
            },
          },
        })
      } else {
        return update(state, {
          alergias: {
            $set: [false, false, false, false, false, true],
          },
        })
      }
    }
    case 'SELECT_BADHABITS': {
      if (action.optionbadhabits !== 5) {
        return update(state, {
          maloshabitos: {
            [action.optionbadhabits]: {
              $set: !state.maloshabitos[action.optionbadhabits],
            },
            [state.maloshabitos.length - 1]: {
              $set: false,
            },
          },
        })
      } else {
        return update(state, {
          maloshabitos: {
            $set: [false, false, false, false, false, true],
          },
        })
      }
    }
    case 'SELECT_OBJETIVE': {
      return update(state, {
        objetivo: {
          $set: action.objetivo,
        },
      })
    }
    case 'CHANGE_EDAD': {
      return {
        ...state,
        edad: action.edad,
      }
    }
    case 'CHANGE_PESO': {
      return {
        ...state,
        peso: action.peso,
      }
    }
    case 'CHANGE_ALTURA': {
      return {
        ...state,
        altura: action.altura,
      }
    }
    case 'CHANGE_CALORIAS_EDITOR': {
      /* SACAMOS EL VALOR DE LAS CALORIAS */
      let calorias =
        state.objetivo === 1
          ? state.calorias_objetivo + action.calorias_editor
          : state.calorias_objetivo - action.calorias_editor
      /* LLEVAMOS EL PESO A KG */
      let peso_kg = state.type_measure === 0 ? state.peso : state.peso / 2.2046
      /* CALCULAMOS LAS GRASAS POR DIA EN CALORIAS */
      let grasa_dia_cal = Math.round(calorias * (25 / 100))
      /* CALCULAMOS LAS PROTEINAS POR DIA EN CALORIAS */
      let proteinas_dia_cal = Math.round(peso_kg * 1.8 * 4)
      /* CALCULAMOS LOS CARBOHIDRATOS POR DIA EN CALORIAS */
      let carbohidratos_dia_cal = calorias - grasa_dia_cal - proteinas_dia_cal
      return {
        ...state,
        calorias_editor: action.calorias_editor,
        grasa_dia_g: Math.round(grasa_dia_cal / 9),
        carbohidratos_dia_g: Math.round(carbohidratos_dia_cal / 4),
        proteinas_percent: 1.8,
        grasa_percent: 25,
      }
    }
    case 'CHANGE_PERCENT_PROTEINAS': {
      /* SACAMOS EL VALOR DE LAS CALORIAS */
      let calorias =
        state.objetivo === 1
          ? state.calorias_objetivo + state.calorias_editor
          : state.calorias_objetivo - state.calorias_editor
      /* LLEVAMOS EL PESO A KG */
      let peso_kg = state.type_measure === 0 ? state.peso : state.peso / 2.2046
      /* CALCULAMOS LAS GRASAS POR DIA EN CALORIAS */
      let grasa_dia_cal = Math.round(state.grasa_dia_g * 9)
      /* CALCULAMOS LAS PROTEINAS POR DIA EN CALORIAS */
      let proteinas_dia_cal = Math.round(peso_kg * action.proteinas_percent * 4)
      /* CALCULAMOS LOS CARBOHIDRATOS POR DIA EN CALORIAS */
      let carbohidratos_dia_cal = calorias - grasa_dia_cal - proteinas_dia_cal
      if (carbohidratos_dia_cal > 0) {
        return update(state, {
          proteinas_dia_g: {
            $set: Math.round(peso_kg * action.proteinas_percent),
          },
          proteinas_percent: {
            $set: action.proteinas_percent,
          },
          carbohidratos_dia_g: {
            $set: Math.round(carbohidratos_dia_cal / 4),
          },
        })
      } else {
        /* CALCULAMOS LAS GRASAS QUE RECOMENDADAS CON LOS VALORES ACTUALES DEL SLIDER */
        let grasa_dia_cal2 = Math.round(calorias * (state.grasa_percent / 100))
        /* LE RESTAMOS LOS CARBOHIDRATOS NEGATIVOS A 
        LAS GRASAS PARA VER EL VALOR DE GRASAS QUE DEBERIAN MOSTRARSE PARA QUE LOS CARBOS NO SEAN 0 */
        let new_grasa_cal = grasa_dia_cal2 + carbohidratos_dia_cal
        /* CALCULAMOS CUAL DEBERIA SER EL PORCENTAGE DE GRASAS PARA QUE DEN EL VALOR ANTERIOR */
        let new_percent = Math.trunc((new_grasa_cal * 100) / calorias)
        /* CALCULAMOS EL VALOR EXACTO DE GRASAS  CON EL PORCENTAJE ANTERIOR */
        grasa_dia_cal2 = Math.round(calorias * (new_percent / 100))
        /* CALCULAMOS LOS NUEVOS CARBOHIDRATOS */
        carbohidratos_dia_cal = calorias - grasa_dia_cal2 - proteinas_dia_cal
        return update(state, {
          proteinas_dia_g: {
            $set: Math.round(peso_kg * action.proteinas_percent),
          },
          grasa_dia_g: {
            $set: Math.round(grasa_dia_cal2 / 9),
          },
          carbohidratos_dia_g: {
            $set: Math.round(carbohidratos_dia_cal / 4),
          },
          grasa_percent: {
            $set: new_percent,
          },
          proteinas_percent: {
            $set: action.proteinas_percent,
          },
        })
      }
    }
    case 'CHANGE_PERCEMT_GRASA': {
      /* SACAMOS EL VALOR DE LAS CALORIAS */
      let calorias =
        state.objetivo === 1
          ? state.calorias_objetivo + state.calorias_editor
          : state.calorias_objetivo - state.calorias_editor

      let peso_kg = state.type_measure === 0 ? state.peso : state.peso / 2.2046
      let grasa_dia_cal = Math.round(calorias * (action.grasa_percent / 100))
      let proteinas_dia_cal = Math.round(peso_kg * state.proteinas_percent * 4)
      let carbohidratos_dia_cal = Math.round(
        calorias - grasa_dia_cal - proteinas_dia_cal
      )

      if (carbohidratos_dia_cal > 0) {
        return update(state, {
          grasa_dia_g: {
            $set: Math.round(grasa_dia_cal / 9),
          },
          carbohidratos_dia_g: {
            $set: Math.round(carbohidratos_dia_cal / 4),
          },
          grasa_percent: {
            $set: action.grasa_percent,
          },
        })
      } else {
        /* CALCULAMOS LAS PROTEINAS CON LOS VALORES ACTUALES DEL SLIDER */
        let proteinas_dia_cal2 = Math.round(
          peso_kg * state.proteinas_percent * 4
        )
        /* LE RESTAMOS LOS CARBOS PARA SABER QUE VALOR DE PROTEINAS DEBERIAMOS TENER */
        let new_proteinas = proteinas_dia_cal2 + carbohidratos_dia_cal
        /* CALCULAMOS CUAL ES EL MAXIMO DE PROTEINAS QUE PUEDE TENER */
        let proteinas_max = Math.round(peso_kg * 3 * 4)
        /* CALCULAMOS EL NUEVO VALOR DEL SLIDER */
        let new_percent = (new_proteinas * 3) / proteinas_max
        new_percent = parseInt(new_percent * 10, 10) / 10
        /* CALCULAMOS EL NUEVO VALOR DE PROTEINAS */
        proteinas_dia_cal2 = Math.round(peso_kg * new_percent * 4)
        /* CALCULAMOS EL NUEVO VALOR DE LOS CARBOS */
        carbohidratos_dia_cal = Math.round(
          calorias - grasa_dia_cal - proteinas_dia_cal2
        )
        return update(state, {
          grasa_dia_g: {
            $set: Math.round(grasa_dia_cal / 9),
          },
          carbohidratos_dia_g: {
            $set: Math.round(carbohidratos_dia_cal / 4),
          },
          grasa_percent: {
            $set: action.grasa_percent,
          },
          proteinas_percent: {
            $set: new_percent,
          },
          proteinas_dia_g: {
            $set: Math.round(proteinas_dia_cal2 / 4),
          },
        })
      }
    }
    case 'CHAGE_PAYVALUE': {
      return {
        ...state,
        totalValue: action.value,
      }
    }
    case 'SET_AL_STATE': {
      return action.data
    }
    case 'GENERATE_RESULT': {
      state = JSON.parse(localStorage.getItem('dataCalc'))
      console.log(state)
      let {
        bmr,
        calorias_nivel,
        bmi,
        peso_ideal_kg,
        mpm_superior,
        mpm_inferior,
        calorias_objetivo,
        proteinas_dia_g,
        grasa_dia_g,
        carbohidratos_dia_g,
        calorias_editor,
      } = calculateResult(state)

      return {
        ...state,
        bmr,
        calorias_nivel,
        bmi,
        peso_ideal_kg,
        mpm_superior,
        mpm_inferior,
        calorias_objetivo,
        proteinas_dia_g,
        grasa_dia_g,
        carbohidratos_dia_g,
        calorias_editor,
      }
    }
    default: {
      return {
        ...state,
      }
    }
  }
}

export default createStore(reducer)
